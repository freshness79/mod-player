// Load indexed data
// *****************
.macro structload(zerop,index)
{
	ldy #index
	lda (zerop),y
}

// Load indexed data
// ***************** 
.macro structloadax(zerop,index)
{
	ldy #index
	lax (zerop),y
}

// Ora indexed data
// ****************
.macro structora(zerop,index)
{
	ldy #index
	ora (zerop),y
}

// Adc indexed data
// ****************
.macro structadc(zerop,index)
{
	ldy #index
	adc (zerop),y
}

// Sbc indexed data
// ****************
.macro structsbc(zerop,index)
{
	ldy #index
	sbc (zerop),y
}

// Cmp indexed data
// ****************
.macro structcmp(zerop,index)
{
	ldy #index
	cmp (zerop),y
}

// Save indexed data
// *****************
.macro structsave(zerop,index)
{
	ldy #index
	sta (zerop),y
}

