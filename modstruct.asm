// Sample pointers
// ***************

.align $20
// List of pointer to sample tables
// ********************************
.print "Sample headers: $"+toHexString(*)
samplelistl:
.fill maxsamplesz,<samples.get(i)
.byte $ff
samplelisth:
.fill maxsamplesz,>samples.get(i)
.byte $ff
