// Description: Unsigned multiplication routines

.const	fpprecbits		=	15				// Fixed point precision bits

// Multiply functions
// ******************

// Generic 8x8 => 16 multiplier
// *****************************
// IN: A = Multiplier
//     Y = Multiplier
// OUT: A = Hi byte of product
//      PRODUCT (2 bytes)
// Clobbered: -
mul8x8:		
	sty SPARE		// 3 Save first multiplier	
	lsr				// 2
	sta PRODUCT		// 3 counter (shift based)
	lda #$00		// 2 Product Hi = 0 	
	ldy #$08		// 2 - 13  
loop:
	bcc !+			// 2/3
	clc				// 2
	adc SPARE 		// 3
!:		
	ror				// 2
	ror PRODUCT		// 5
	dey				// 2
	bne loop		// 2/3 - WORST: 19*7+18 => 143 / BEST: 15*7+14 => 119
	sta PRODUCT+1	// 3
	ldy SPARE		// 3
	rts				// 6 - 10 => WORST: 175 / BEST: 145
	
	
// Input: 16-bit unsigned value in ARG1                                      
//        16-bit unsigned value in ARG2                                      
//                                                                         
// Output: 32-bit unsigned value in PRODUCT                                
//                                                                         
// Clobbered: PRODUCT, X, A, C
//                                                                         
// Allocation setup: ARG1,ARG2 and PRODUCT preferably on Zero-page.            
mult16x16:			
	sty SPARE
	lda ARG2
	sta PRODUCT+2
	lda ARG2+1
	sta PRODUCT+3
	lda #$00	
	sta PRODUCT+1		
	ldx #$10
m16loop:	
	asl				// 2
	rol PRODUCT+1	// 5
	rol PRODUCT+2	// 5
	rol PRODUCT+3	// 5
	bcc !+			// 2/3
	clc				// 2	
	adc ARG1		// 3
	tay				// 2
	lda PRODUCT+1	// 3
	adc ARG1+1		// 3
	sta PRODUCT+1	// 3
	tya				// 2
	bcc !+			// 2/3
	inc PRODUCT+2	// 5
	bne !+			// 2/3
	inc PRODUCT+3	// 5
!:	dex				// 2
	bne m16loop		// 3	- 37
	.for(var i=0;i<[16-fpprecbits];i++) {
			asl
			rol PRODUCT+1
			rol PRODUCT+2
			rol PRODUCT+3
	}
	sta PRODUCT
savey:	
	ldy SPARE
	rts				
				

// 24bit/16bit slow division (taken from codebase)
// ***********************************************
divide24x16:
	lda #0	        	//preset remainder to 0
	sta SPARE
	sta SPARE+1
	ldx #24	        	//repeat for each bit: ...
divloop:	
	asl DIVIDEND	//dividend lb & hb*2, msb -> Carry
	rol DIVIDEND+1
	rol DIVIDEND+2
	rol SPARE		//remainder lb & hb * 2 + msb from carry
	rol SPARE+1
	lda SPARE
	sec
	sbc DIVISOR		//substract divisor to see if it fits in
	tay	        	//lb result -> Y, for we may need it later
	lda SPARE+1
	sbc DIVISOR+1
	bcc skip		//if carry=0 then divisor didn't fit in yet

	sta SPARE+1		//else save substraction result as new remainder,
	sty SPARE
	inc DIVIDEND	// and INCrement result cause divisor fit in 1 times
skip:	
	dex
	bne divloop	
	rts				
