// Strings
// *******	

.align $04
.pc=* "Strings"
orgstartstrings:
orgstrnotes:
.text "..."
.byte $00
.text "C-3"
.byte $00
.text "C#3"
.byte $00
.text "D-3"
.byte $00
.text "D#3"
.byte $00
.text "E-3"
.byte $00
.text "F-3"
.byte $00
.text "F#3"
.byte $00
.text "G-3"
.byte $00
.text "G#3"
.byte $00
.text "A-3"
.byte $00
.text "A#3"
.byte $00
.text "B-3"
.byte $00
.text "C-4"
.byte $00
.text "C#4"
.byte $00
.text "D-4"
.byte $00
.text "D#4"
.byte $00
.text "E-4"
.byte $00
.text "F-4"
.byte $00
.text "F#4"
.byte $00
.text "G-4"
.byte $00
.text "G#4"
.byte $00
.text "A-4"
.byte $00
.text "A#4"
.byte $00
.text "B-4"
.byte $00
.text "C-5"
.byte $00
.text "C#5"
.byte $00
.text "D-5"
.byte $00
.text "D#5"
.byte $00
.text "E-5"
.byte $00
.text "F-5"
.byte $00
.text "F#5"
.byte $00
.text "G-5"
.byte $00
.text "G#5"
.byte $00
.text "A-5"
.byte $00
.text "A#5"
.byte $00
.text "B-5"
.byte $00
orgstrdet:
.text "Detecting REU... "
.byte 0
orgstrdetaud:
.text "Detecting Audio Module... "
.byte 0
orgstrreufail:
.text "FAIL"
.byte 0
orgstrreuok:
.text "OK"
.byte 0
orgstrreusize:
.text " - Size: "
.byte 0
orgstraudver:
.text " - V."
.byte 0
orgstrname:
.text "Module name: "
.byte 0
orgstrminus:
.text ":"
.byte 0
orgstrsonglenght:
.text "Song length: "
.byte 0
orgstrpatternnumber:
.text "Pattern number: "
.byte 0
orgstrsongplayptn:
.text "Pattern: " 
.byte 0
orgstrsongplaydiv:
.text "Division: "
.byte 0
orgstrmodnotsupp:
.text "Module not supported..."
.byte 0
orgstrreukb:
.text "kb"
.byte 0
orgstrreu0:
.text "128"
.byte 0
orgstrreu1:
.text "256"
.byte 0
orgstrreu2:
.text "512"
.byte 0
orgstrreu3:
.text "1024"
.byte 0
orgstrreu4:
.text "2048"
.byte 0
orgstrreu5:
.text "4096"
.byte 0
orgstrreu6:
.text "8192"
.byte 0
orgstrreu7:
.text "16384"
.byte 0
orgstrvolume:
.text "Main volume:"
.byte 0
orgstrchn1:
.text "CHAN.1"
.byte 0
orgstrchn2:
.text "CHAN.2"
.byte 0
orgstrchn3:
.text "CHAN.3"
.byte 0
orgstrchn4:
.text "CHAN.4"
.byte 0
orgstrmono:
.text "MO"
.byte 0
orgstrstereo:
.text "ST"
.byte 0
orgstrtitle:
.text "-=1541U2-Audio MOD Player by Freshness=-"
.byte 0
orgendstrings:
