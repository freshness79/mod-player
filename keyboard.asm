//*********************************************************
// Routine for reading keyboard or joy port 1 independently
//*********************************************************
// $00  => No input
// $00-$1f	=> Joystick
// $80-$ff	=> Keyboard

.const	cia1pra		=	$dc00
.const	cia1prb		=	$dc01
.const	cia1ddra	=	$dc02
.const	cia1ddrb	=	$dc03

GetKeyJoy:
	lda #$7F
	sta GKJtemp
	ldy #$ff	
	sty cia1ddra	
	lda #$00
	sta cia1ddrb  
	sec
GKJreadcia:  
	ldx GKJtemp  	
	sty cia1pra
	lda cia1prb
	stx cia1pra
	ldx cia1prb
	sty cia1pra
	and cia1prb	
	eor #$ff
	bne GKJIsJoy
	txa
	eor #$ff
	bne GKJIsKey  
	ror GKJtemp	
	bcs GKJreadcia
GKJIsJoy:
	ldx #$00
	stx $dc00	
	and #$ff
	rts  
GKJIsKey: 
	lsr GKJtemp
	iny
	bcs GKJIsKey	
	sty GKJtemp
	asl GKJtemp
	asl GKJtemp
	asl GKJtemp	
	ldy #$ff
GKJFindKey:
	iny
	lsr
	bcc GKJFindKey
	tya
	ora GKJtemp
	eor #$80
	bne GKJIsJoy	

// Key routine
// ***********
keyroutine:
	jsr GetKeyJoy		
	cmp curkey
	sei
	bne firstpress	
	sta curkey	
	cmp #$00
	beq nokey	
	ldx keyrepeat	
	cmp curkey
	bne firstpress	
	dex
	stx keyrepeat
	bne nokey	
	ldx #$03
	stx keyrepeat		
	jsr parsekey	
nokey:
	cli
	rts
firstpress:
	sta curkey
	ldx #$08
	stx keyrepeat
	jsr parsekey	
	cli
	rts
	
parsekey:
	cmp #$84
	bne !+
	ldx #$00
	lda #<buffer1
	sta bufferptr
	jmp togglevoice	
!:	
	cmp #$85
	bne !+
	ldx #$01
	lda #<buffer2
	sta bufferptr
	jmp togglevoice	
!:
	cmp #$86
	bne !+
	ldx #$02
	lda #<buffer3
	sta bufferptr
	jmp togglevoice	
!:
	cmp #$83
	bne !+
	ldx #$03
	lda #<buffer4
	sta bufferptr
	jmp togglevoice		
!:
	cmp #$BE
	bne !+
	ldx mainvolume
	cpx #$3f
	beq novolinc
	inx
	stx mainvolume
	jmp setmainvolume
novolinc:
	rts
!:
	cmp #$8A
	bne !+
	ldx mainvolume	
	beq !+
	dex
	stx mainvolume
	jmp setmainvolume
!:
	cmp #$8D
	bne !+
	jmp togglemono
!:
	rts	

// Toggle voice
// ************	
togglevoice:
	lda #$ff
	sta keyrepeat
	:structload(bufferptr,sftflgoff)
	eor #$01
	:structsave(bufferptr,sftflgoff)
	lsr
	bcc tvoff
	:structload(bufferptr,audvol)	
tvoff:	
	bcs tvon
	:structload(bufferptr,sftcmdoff)	
	and #[$ff-flagvolume]
	:structsave(bufferptr,sftcmdoff)	
	lda #$00
	beq !+
tvon:	
	:structload(bufferptr,audvol)	
!:
	jmp	setvol	

	
// Set main volume
// ***************
setmainvolume:	
	lda #<buffer1
	sta bufferptr
	ldx #$00
	jsr setvoicevolume
	lda #<buffer2
	sta bufferptr
	ldx #$01
	jsr setvoicevolume
	lda #<buffer3
	sta bufferptr
	ldx #$02
	jsr setvoicevolume
	lda #<buffer4
	sta bufferptr
	ldx #$03
	jsr setvoicevolume	
	rts
	
// Set main volume
// ***************
setvoicevolume:
	:structload(bufferptr,sftflgoff)	// Check if active
	and #$01
	beq !+
	:structload(bufferptr,sftcmdoff)	
	and #[$ff-flagvolume]
	:structsave(bufferptr,sftcmdoff)	
	:structload(bufferptr,sftvoloff)
	tay	
	lda mainvolume
	asl
	asl
	jsr mul8x8
	lda PRODUCT+1
	:structsave(bufferptr,audvol)	
	jmp setvol
!:
	rts
	
:structload(bufferptr,sftvoloff)	// Adapt channel volume to main volume value
	tay
	lda mainvolume
	asl
	asl
	jsr mul8x8
	lda PRODUCT+1
	:structsave(bufferptr,audvol)		
	

// Set mono mode
// *************
togglemono:	
	ldx #$ff
	stx keyrepeat		
	lda #$00
	sta bufferptr
	lda monomode
	eor #$01
	sta monomode
	lsr
	bcs !+	
	lda #$08
	ldx #$00	
	:structsave(bufferptr,<buffer1+sftpanoff)
	:structsave(bufferptr,<buffer1+audpan)
	jsr setpan
	ldx #$01	
	:structsave(bufferptr,<buffer2+sftpanoff)
	:structsave(bufferptr,<buffer2+audpan)
	jsr setpan
	ldx #$02	
	:structsave(bufferptr,<buffer3+sftpanoff)
	:structsave(bufferptr,<buffer3+audpan)
	jsr setpan
	ldx #$03
	:structsave(bufferptr,<buffer4+sftpanoff)	
	:structsave(bufferptr,<buffer4+audpan)
	jsr setpan
	rts
!:	
	lda #$00
	ldx #$00	
	:structsave(bufferptr,<buffer1+sftpanoff)	
	:structsave(bufferptr,<buffer1+audpan)	
	jsr setpan
	ldx #$01	
	:structsave(bufferptr,<buffer2+sftpanoff)	
	:structsave(bufferptr,<buffer2+audpan)	
	jsr setpan
	lda #$0f
	ldx #$02	
	:structsave(bufferptr,<buffer3+sftpanoff)	
	:structsave(bufferptr,<buffer3+audpan)
	jsr setpan
	ldx #$03
	:structsave(bufferptr,<buffer4+sftpanoff)	
	:structsave(bufferptr,<buffer4+audpan)	
	jsr setpan
	rts	

	