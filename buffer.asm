// Buffered mod player
// *******************
	
// Parse channels	
// **************	
playdivision:

	lda #<buffer1	// Voice 1
	sta bufferptr
	lda #<division
	sta chanptr	
	jsr playchannel			
	lda #<buffer2	// Voice 2
	sta bufferptr
	lda #<division+4
	sta chanptr	
	jsr playchannel
	lda #<buffer3	// Voice 3
	sta bufferptr
	lda #<division+8
	sta chanptr		
	jsr playchannel	
	lda #<buffer4	// Voice 4
	sta bufferptr
	lda #<division+12
	sta chanptr	
	jmp playchannel		


// Play generic channel
// ********************	
// Play mod on buffer
playchannel:
	// Load data from channel
	ldx #$0f
	:structload(chanptr,0)	// Byte 0
	sax temp4							// Rate HI
	and #$f0
	sta temp2
	:structload(chanptr,1)	// Byte 1
	sta temp3							// Rate LO
	:structload(chanptr,2)	// Byte 2
	sax temp
	lsr
	lsr
	lsr
	lsr
	ora temp2	
	sta	temp5							// Sample number
	:structload(chanptr,3)	// Byte 3	
	:structsave(bufferptr,sfttxdoff)	// FX data
	lda temp
	:structsave(bufferptr,sfttfxoff)	// FX number	
	lda temp4
	ldx temp3	
	jsr getnote							// Translate period to note (logaritmic to linear)
	:structsave(bufferptr,sftnotoff)	// Save note
	
	// Check sample data
	
	lax temp5	
	bne !+	
	jmp nonewsample
!:	
	:structsave(bufferptr,sftsmpoff)	// New sample to be played: fill
	dex									// registers according to sample data
	lda #$01							// Memory position
	:structsave(bufferptr,audsms)		
	lda sampleptrh,x	
	:structsave(bufferptr,audsms+1)
	lda sampleptrm,x
	:structsave(bufferptr,audsms+2)
	lda sampleptrl,x
	:structsave(bufferptr,audsms+3) 
	lda samplelenh,x					// Lenght
	:structsave(bufferptr,audsml)
	lda samplelenm,x
	:structsave(bufferptr,audsml+1)
	lda samplelenl,x
	:structsave(bufferptr,audsml+2)
	lda samplerep,x
	beq norep
	:structload(bufferptr,sftcmdoff)	// Enable ** REPEAT **
	ora #flagrepeat
	:structsave(bufferptr,sftcmdoff)
	lda samplerepah,x					// Repeat position A
	:structsave(bufferptr,audrpa)
	lda samplerepam,x
	:structsave(bufferptr,audrpa+1)
	lda samplerepal,x
	:structsave(bufferptr,audrpa+2)
	lda samplerepbh,x
	:structsave(bufferptr,audrpb)		// Repeat position B
	lda samplerepbm,x
	:structsave(bufferptr,audrpb+1)	
	lda samplerepbl,x
	:structsave(bufferptr,audrpb+2)

	lda #$02
norep:
	ora #$01
	:structsave(bufferptr,audctr)
	:structload(bufferptr,sftcmdoff)	// Enable ** NEW SAMPLE ** & ** VOLUME ** & ** PANNING **
	ora #[flagvolume|flagpanning]
	:structsave(bufferptr,sftcmdoff)
	lda samplevol,x			// Set vol	
	:structsave(bufferptr,sftvoloff)
	:structload(bufferptr,sftpanoff)	
	:structsave(bufferptr,audpan)	
nonewsample:	
	
	// Check rate data
		
	:structload(bufferptr,sftnotoff)
	beq !+	
	:structsave(bufferptr,sftlntoff)
	:structloadax(bufferptr,sftsmpoff)
	dex
	lda samplerep,x
	beq ratenorep
	:structload(bufferptr,sftcmdoff)	// Enable ** SAMPLE RATE **
	ora #flagrepeat
	:structsave(bufferptr,sftcmdoff)	
ratenorep:
	:structload(bufferptr,sftcmdoff)	// Enable ** SAMPLE RATE **
	ora #flagnewsmp|flagrate
	:structsave(bufferptr,sftcmdoff)
	:structload(bufferptr,sftnrtoff)	// Save old rate value
	:structsave(bufferptr,sftortoff)
	:structload(bufferptr,sftnrtoff+1)
	:structsave(bufferptr,sftortoff+1)
	:structload(bufferptr,sftnotoff)
	sta temp	
	lda sampleft,x						// A = Finetune
	ldx temp							// X = note
	jsr getrate							// Get rate from note & finetune
	:structsave(bufferptr,sftnrtoff+1)	// 
	txa
	:structsave(bufferptr,sftnrtoff)	// Save new rate value
	jsr checkwaves						// Check/update tremolo & vibrato positions
	lda #$01
!:					
	:structsave(bufferptr,sftnntoff)	// New note!
	
	rts
	

// Reset vibrato/tremolo position if needed
// ****************************************	
checkwaves:	
	:structload(bufferptr,sftvbwoff)	// Fix vibrato position
	and #$08
	bne !+
	:structsave(bufferptr,sftvbpoff)
!:
	:structload(bufferptr,sfttrwoff)	// Fix tremolo position
	and #$08
	bne !+
	:structsave(bufferptr,sfttrpoff)	
!:
	rts

	
// Clear buffers routine
// *********************
clearbuffers:
	ldx #$40-1
	lda #$00
!:
	sta buffer1,x
	sta buffer2,x
	sta buffer3,x
	sta buffer4,x
	dex
	bpl !-
	rts	
	
// Final check (pre-play)
// **********************
finalcheck:
	lda #<buffer1	// Voice 1
	sta bufferptr
	jsr channelfinalcheck			
	lda #<buffer2	// Voice 2
	sta bufferptr
	jsr channelfinalcheck		
	lda #<buffer3	// Voice 3
	sta bufferptr
	jsr channelfinalcheck	
	lda #<buffer4	// Voice 4
	sta bufferptr
	jsr channelfinalcheck			
	rts
	
// Final check
// ***********
channelfinalcheck:
	:structload(bufferptr,sftcmdoff)
	and #flagrate						// If sample updated
	beq !+	
	:structloadax(bufferptr,sftnrtoff)	
	:structload(bufferptr,sftnrtoff+1)
	tay	
	jsr fixperiod						// Translate amiga period to audio module period
	lda PRODUCT+2
	:structsave(bufferptr,audrat+1)	
	lda PRODUCT+3
	:structsave(bufferptr,audrat)	
	jmp !+
	
!:
	:structload(bufferptr,sftcmdoff)
	and #flagvibrato					// If doing vibrato
	beq !+	
	:structload(bufferptr,sftcmdoff)
	eor #flagvibrato|flagrate	
	:structsave(bufferptr,sftcmdoff)	// Just update rate
	jmp !+	
	
!:
	:structload(bufferptr,sftcmdoff)
	and #flagvolume						// If volume updated
	beq !+
	:structload(bufferptr,sftvoloff)	// Adapt channel volume to main volume value
	tay
	lda mainvolume
	asl
	asl
	jsr mul8x8
	lda PRODUCT+1
	:structsave(bufferptr,audvol)	
!:
	:structload(bufferptr,sftflgoff)	// Check if active
	and #$01
	bne !+
	lda #$00	
	:structsave(bufferptr,audvol)
	:structload(bufferptr,sftcmdoff)
	ora #flagvolume						// If volume updated	
	:structsave(bufferptr,sftcmdoff)
!:
	lda monomode
	lsr
	bcs !+
	:structload(bufferptr,sftcmdoff)
	and #[$ff-flagpanning]				
	:structsave(bufferptr,sftcmdoff)	
!:
	rts
	
