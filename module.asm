// Machine type data

.const	pal		=	true		// PAL/NTSC?

// Main song constants

.const	emu			=	true	// Set to true if trying on emulators (Test)
.const	test		=	true	// Set for normal mode (No cartridge)
.const	raster		=	false	// Set to true to show timing raster bars
.const	c64			=	pal
.const	maxsamplesz	=	31		// Max number of instruments
.const	defbpm		=	125		// Default beat per minute
.const	deftick		=	6		// Default tick per division

// Screen rows

.const	ydetect1	=	0
.const	ydetect2	=	1
.const	ydetect3	=	2
.const	ytitle		=	2
.const	yname		=	3
.const	ysnglenght	=	4
.const	yptnnumber	=	4
.const	ysonginfo	=	5
.const	ysample		=	6
.const	yvolinfo	=	21
.const	ystereo		=	21
.const	ychanhead	=	23
.const	ynoteinfo	=	24

// Colors

.const	col1		=	$07
.const	col2		=	$01
.const	col3		=	$06
.const	col4		=	$0e
.const	col5		=	$0b
.const	col6		=	$02
.const	col7		=	$08

// Memory layout

.const	varbase		=	$c000
.const	codebase	=	$8000
.const	charscreen	=	$0400
.const	charset		=	$0800

// Specific variables

.const	namesz	=	20
.const	singlesz=	30
.const	samplesz31	=	singlesz*31
.const	samplesz15	=	singlesz*15
.const	songsz	=	130

// *************
// * Zero Page *
// *************

// General purpose variables
// *************************
.const	zerop1	=	$f0

.const	temp	=	zerop1+$00
.const	temp2	=	zerop1+$01
.const	temp3	=	zerop1+$02
.const	temp4	=	zerop1+$03
.const	temp5	=	zerop1+$04
.const	counter	=	zerop1+$05
.const	arg1	=	zerop1+$06
.const	arg2	=	zerop1+$07
.const	arg3	=	zerop1+$08
.const	color	=	zerop1+$09
.const	pointer	=	zerop1+$0A	// 6 Bytes


// Module pointers
// ***************
.const	zerop2	=	$e0

.const	bufferptr	=	zerop2+$00	// 2 bytes pointer
.const	chanptr		=	zerop2+$02	// 2 bytes pointer
.const	patternptr	=	zerop2+$04	// 2 bytes pointer
.const	songptr		=	zerop2+$06	// 2 bytes pointer
.const	nsamples	=	zerop2+$08	// Actual number of samples
.const	songlenght	= 	zerop2+$09	// Song lenght
.const	mainvolume	=	zerop2+$0a	// Main volume
.const	monomode	=	zerop2+$0f	// Stereo/Monaural mode


// Math
// ****
.const	zerop3	=	$d0

.const	PRODUCT		= zerop3+$00	// 4 bytes result
.const	ARG1		= zerop3+$04	// 2 bytes operand
.const	ARG2		= zerop3+$06	// 2 bytes operand
.const	DIVISOR		= zerop3+$08	// 2 bytes operand
.const	DIVIDEND	= zerop3+$0A	// 3 bytes operand
.const	SPARE		= zerop3+$0D	// 3 bytes operand


// Various
// *******
.const	zerop4	=	$c0

.const	patdelay		= zerop4+$00	// Pattern delay
.const	patternsnumber	= zerop4+$01	// Number of patterns
.const	curpattern		= zerop4+$02	// Current patterns
.const	patternindex	= zerop4+$03	// Current pattern index
.const	curdivision		= zerop4+$04	// Current division
.const	curtick			= zerop4+$05	// Current tick position
.const	reusize			= zerop4+$06	// 128kb banks REU size
.const	printlimit		= zerop4+$07	// Print string max char (>=1)
.const	skipdivision	= zerop4+$08	// Skip autoincrement division if set (used for pattern jump)
.const	newmod			= zerop4+$09	// Detect REU changes
.const	timer			= zerop4+$0a	// 2 bytes
.const	changetimer		= zerop4+$0c	// 1 byte (flag)
.const	newspeed		= zerop4+$0d	// 1 byte
.const	tickperdiv		= zerop4+$0e	// Default tick number per division (Def 6)
.const	bpm				= zerop4+$0f	// Beat per minute

.const	zerop5	=	$b0

.const	hinib			= zerop5+$00	// Hi nibble mask ($f0)
.const	lonib			= zerop5+$01	// Lo nibble mask ($0f)
.const	tag				= zerop5+$02	// 4 if tag present, 0 if not present


// Player value
// ************

.const	maxslideperiod	=	856
.const	minslideperiod	=	113
.const	reset			=	$fce2

.if (test==true) {
	:BasicUpstart2(start)
}

.pc=codebase "Base"

// Reset magic bytes
// *****************
.byte	<start,>start
.byte	<nmi,>nmi
.byte	$c3,$c2,$cd,$38,$30
nmi:
	rti

.import	source "variables.asm"		// Variables
.import source "audiodef.asm"		// Symbol definitions for 1541U2 Audio module
.import source "bufferdef.asm"		// Symbol definitions for play buffer
.import source "macro.asm"			// :structxxxxx macros
.import source "math.asm"			// Some multiplication and one division
.import source "audio.asm"			// 1541U2 Audio module specific routines
.import source "reu_routines.asm"	// REU routines
.import source "functions.asm"		// Miscellaneous function (mainly screen print)
.import source "buffer.asm"			// Buffer playing functions
.import source "periodfix.asm"		// Fix timing of DAC and play routine
.import source "loader.asm"			// Load module and fill data structures
.import	source "fx.asm"				// Parse and exec speciale effects
.import source "timerirq.asm"		// IRQ stuff
.import	source "keyboard.asm"		// Keyboard routines
.import source "modstruct.asm"		// Specific MOD data structures
.import	source "strings.asm"		// Strings to be printed on screen

.pc=* "Main routine"

// Main routine
// ************
start:		
	sei	
	jsr $fda3	// Prepare IRQ
	jsr $fd50	// Init memory
	jsr $fd15	// Init I/O
	jsr $ff5b	// Init video
	lda #$00
	sta $d020
	sta $d021	
	cli
	.if (test) {
		lda #$36
		sta $01
	}
	jsr disablenmi	// Disable NMI
	jsr setfakeirq	// Don't MOVE!!
	jsr	copycharset	// Copy charset to RAM
	jsr initscreen	// Clear screen and change font		
	jsr initvar		// Miscellaneous init
	jsr reucheck	// Check REU
	bcc exit
	jsr audiocheck	// Check audio module	
	.if (!emu)	bcc exit
	jsr loadmodule	// Load module header block
	jsr modcheck	// Check module format
	bcc exit
	jsr resetaudio	// Reset audio module
	jsr setmodstructures	// Fill player data structures
	jsr printinfo			// Print some info about the mod	
	jsr setmainirq			// Start!
mainloop:
	lsr newmod		// New mod flag	
	bcs start
	jsr keyroutine
!:
	bit $d011
	bpl	!-
!:
	bit $d011
	bmi	!-	
	bpl mainloop

// Exit
// ******
exit:
	jsr setstdirq
	lda #$00
	sta $c6	
!:
	lda $c6
	beq !-	
	jmp reset
	rts
	
	
// *****************************
// ** Initialization routines **
// *****************************
	
// Init miscellaneous variables
// ****************************	
initvar:
	jsr fixstrings	
	lda #$00
	sta songlenght
	sta patternsnumber
	sta curpattern
	sta patternindex
	sta curdivision
	sta reusize
	sta printlimit
	sta skipdivision	
	sta newmod
	sta patdelay
	lda #$01
	sta curtick
	lda #>division
	sta chanptr+1
	lda #>buffers
	sta bufferptr+1	
	jsr clearbuffers
	lda #$00
	sta bufferptr	
	lda #$30		// Set initial general volume
	sta mainvolume
	lda #$00		// Set initial panning (from left to right)
	:structsave(bufferptr,<buffer1+sftpanoff)	
	:structsave(bufferptr,<buffer2+sftpanoff)
	lda #$0f
	:structsave(bufferptr,<buffer3+sftpanoff)	
	:structsave(bufferptr,<buffer4+sftpanoff)
	lda #$01
	:structsave(bufferptr,<buffer1+sftflgoff)
	:structsave(bufferptr,<buffer2+sftflgoff)
	:structsave(bufferptr,<buffer3+sftflgoff)
	:structsave(bufferptr,<buffer4+sftflgoff)
	jsr clearsampleinfo
	jsr	clearmoduledata
	lda #$01
	sta changetimer
	lda #deftick
	sta newspeed
	lda #defbpm
	sta bpm
	jsr calctimer
	sta timer
	stx timer+1
	lda #$01
	sta monomode
	lda #$00
	sta	GKJtemp
	lda #$04
	sta keyrepeat
	lda #$00
	sta curkey
	lda #JMP_IND
	sta fxjump
	lda #<fxtablet0
	sta fxjump+1
	lda #>fxtablet0
	sta fxjump+2
	lda #$0f
	sta lonib
	lda #$f0
	sta hinib
	rts
	
	
// Check REU and print info on screen
// **********************************
reucheck:	
	lda #col1
	sta color
	lda #<strdet
	sta arg1
	lda #>strdet
	sta arg2
	ldx #$00
	ldy #ydetect1
	jsr printstring
	stx temp2
	jsr reu_detect	
	ldx temp2
	ldy #ydetect1
	bcs !+
	jmp reunotfound	
!:	
	pha
	lda #col2
	sta color
	lda #<strreuok
	sta arg1
	lda #>strreuok
	sta arg2		
	jsr printstring	
	lda #col1
	sta color
	lda #<strreusize
	sta arg1
	lda #>strreusize
	sta arg2		
	ldy #ydetect1	
	jsr printstring		
	lda #col2
	sta color
	pla
	sta reusize
	ldy #$00
!:
	lsr
	bcs !+
	iny
	bcc !-
!:	
	lda strreutbll,y
	sta arg1
	lda strreutblh,y
	sta arg2	
	ldy #ydetect1
	jsr printstring	
	lda #<strreukb
	sta arg1
	lda #>strreukb
	sta arg2		
	ldy #ydetect1
	jsr printstring		
	sec
	rts

reunotfound:
	lda #col2
	sta color	
	lda #<strreufail
	sta arg1
	lda #>strreufail
	sta arg2	
	jsr printstring	
	clc
	rts

// Check Audio module and print info on screen
// *******************************************
audiocheck:	
	lda #col1
	sta color
	lda #<strdetaud
	sta arg1
	lda #>strdetaud
	sta arg2
	ldx #$00
	ldy #ydetect2
	jsr printstring
	stx temp	
	jsr detectaudio
	sta	temp2
	lda #col2
	sta color
	ldx temp
	ldy #ydetect2
	bcs !+
	jmp audionotfound	
!:	
	lda #<strreuok
	sta arg1
	lda #>strreuok
	sta arg2		
	jsr printstring	
	lda #<straudver
	sta arg1
	lda #>straudver
	sta arg2	
	jsr printstring			
	lda temp2	
	jsr print2digit
	sec
	rts

audionotfound:	
	lda #<strreufail
	sta arg1
	lda #>strreufail
	sta arg2	
	jsr printstring	
	clc
	rts

// Check Mod file and print info on screen
// ***************************************
modcheck:	
	jsr detectmod
	bcs !+
	ldx #$00
	lda #<strmodnotsupp	
	sta arg1
	lda #>strmodnotsupp	
	sta arg2		
	ldy #ydetect3	
	jsr printstring		
	clc
!:
	rts
	
// Print module info
// *****************
printinfo:			
	lda nsamples
	lsr
	clc
	adc #$01
	sta temp5	
	lda #col1
	sta color	
	ldx #$00
	ldy #yname
	lda #<strname
	sta arg1
	lda #>strname
	sta arg2
	jsr printstring		
	lda #col2
	sta color	
	lda #<mod_name
	sta arg1
	lda #>mod_name
	sta arg2
	jsr printstring		
	lda #col1
	sta color
	lda #$11
	sta printlimit
	lda #ysample
	sta counter
	ldy #$00
	!:	
		sty temp2		
		iny
		lda #col3
		sta color	
		tya
		ldx #$00		
		ldy counter		
		jsr print2digit
		lda #col4
		sta color	
		lda #<strminus
		sta arg1
		lda #>strminus
		sta arg2	
		jsr printstring		
		ldy temp2
		lda samplelistl,y
		sta arg1
		lda samplelisth,y
		sta arg2	
		ldy counter
		jsr printstring
		inc counter
		ldy temp2
		iny		
		cpy temp5
		bcc !-	
	lda #ysample
	sta counter	
	!:	
		sty temp2
		iny
		lda #col3
		sta color	
		tya
		ldx #$14
		ldy counter		
		jsr print2digit
		lda #col4
		sta color	
		lda #<strminus
		sta arg1
		lda #>strminus
		sta arg2	
		jsr printstring		
		ldy temp2
		lda samplelistl,y
		sta arg1
		lda samplelisth,y
		sta arg2	
		ldy counter
		jsr printstring
		inc counter
		ldy temp2
		iny		
		cpy nsamples
		bcc !-
	ldx #$00
	ldy #ysnglenght
	lda #<strsonglenght
	sta arg1
	lda #>strsonglenght
	sta arg2
	lda #col1
	sta color	
	jsr printstring					
	lda #col2
	sta color	
	lda songlenght	
	jsr print2digit	
	ldx #20
	ldy #yptnnumber
	lda #<strpatternnumber
	sta arg1
	lda #>strpatternnumber
	sta arg2
	lda #col1
	sta color	
	jsr printstring					
	lda #col2
	sta color	
	lda patternsnumber
	jsr print2digit
	lda #$20
	sta printlimit
	ldy #ysonginfo
	ldx #$00
	lda #<strsongplayptn
	sta arg1
	lda #>strsongplayptn
	sta arg2
	lda #col1
	sta color	
	jsr printstring
	ldy #ysonginfo
	ldx #20
	lda #<strsongplaydiv
	sta arg1
	lda #>strsongplaydiv
	sta arg2
	lda #col1
	sta color	
	jsr printstring		
	ldy #yvolinfo
	ldx #22
	lda #<strvolume
	sta arg1
	lda #>strvolume
	sta arg2
	lda #col1
	sta color		
	jsr printstring		
	lda #<strchn1
	sta arg1
	lda #>strchn1
	sta arg2
	ldy #ychanhead	
	ldx #02
	lda #col1
	sta color		
	jsr printstring
	lda #<strchn2
	sta arg1
	lda #>strchn2
	sta arg2	
	ldx #12
	lda #col1
	sta color		
	jsr printstring	
	lda #<strchn3
	sta arg1
	lda #>strchn3
	sta arg2
	ldx #22
	lda #col1
	sta color	
	jsr printstring	
	lda #<strchn4
	sta arg1
	lda #>strchn4
	sta arg2
	ldx #32
	lda #col1
	sta color		
	jsr printstring	
	lda #40
	sta printlimit	
	lda #<strtitle
	sta arg1
	lda #>strtitle
	sta arg2	
	ldy #ytitle
	ldx #00
	lda #col7
	sta color		
	jsr printstring	
	rts

	
// ***********************************
// ** Routines called while playing **
// ***********************************
	
// Main player code
// ****************
.print "Main play routine (Playmodule): $"+toHexString(*)
playmodule:
	.if (raster)	inc $d020	
	jsr fastplay		// Move play buffer to Audio Module registers
	dec curtick
	bne skipnote
	lsr changetimer
	bcc notimerchg
	jsr settimer		// If needed change timer
notimerchg:
	lda tickperdiv		// Reset tick counter
	sta curtick	
	lda patdelay
	bne pmdelay
	jsr loaddivision	// Load division from REU	
	jsr playdivision	// Parse data and 'play' onto buffer
	jsr tickfx			// Compute FX
	jsr nextdivision	// Move to next division			
	jsr	finalcheck		// Adjust values for this machine (volume/frequency)	
	jsr detectnewmod	// Test REU for new mods (not always working but... well, it often work!)
	.if (raster)	dec $d020	
	rts
skipnote:
	jsr tickfx			// Compute FX
skipfx:
	jsr	finalcheck		// Adjust values for this machine (volume/frequency)
	jsr detectnewmod	// Test REU for new mods (not always working but... well, it often work!)
	.if (raster)	dec $d020	
	rts
pmdelay:				// Needed for effect EE
	dec patdelay
	jmp skipfx
		
// Load single division
// ********************
loaddivision:	
	lda #$00
	sta temp2
	lda curpattern
	sta temp
	lda curdivision
	asl
	asl
	asl
	rol temp
	rol temp2
	asl
	rol temp
	rol temp2	
	adc patternptr
	tax
	lda temp
	adc patternptr+1
	tay
	lda temp2
	adc #$00	
	jsr reu_loadfix16
	rts

// Load single division
// ********************
detectnewmod:
	ldx #<mod_name
	ldy #>mod_name	
	jsr reu_cmpfix20
	beq !+
	inc newmod
!:	
	rts	
	
// Move to next division
// *********************
nextdivision:
	ldx curdivision
	lsr skipdivision	
	bcs ndskip	
	inx
	cpx #$40
	bcc ndskip
	ldy patternindex
	iny
	jsr setpattern
	lda #$00
	sta bufferptr	// Reset loop
	:structsave(bufferptr,<buffer1+sftlpcoff)	
	:structsave(bufferptr,<buffer2+sftlpcoff)	
	:structsave(bufferptr,<buffer3+sftlpcoff)	
	:structsave(bufferptr,<buffer4+sftlpcoff)	
	ldx #$00
ndskip:	
	stx curdivision
	rts
	
	
// Set pattern
// ***********
setpattern:
	cpy songlenght
	bcc !+
	ldy #$00
!:
	sty patternindex
	lda (songptr),y		
	sta curpattern
	rts	
	
// Print info about playing status
// *******************************	
printdata:	
	lda #$00
	sta bufferptr
	ldy #ysonginfo	
	ldx #9
	lda #col2
	sta color	
	lda curpattern
	jsr print2digit		
	ldx #30
	lda #col2
	sta color		
	lda curdivision
	jsr print2digit		
	lda #<buffer1
	sta bufferptr
	ldx #01
	jsr printplayingnote
	lda #<buffer2
	sta bufferptr
	ldx #11
	jsr printplayingnote
	lda #<buffer3
	sta bufferptr
	ldx #21
	jsr printplayingnote
	lda #<buffer4
	sta bufferptr
	ldx #31
	jsr printplayingnote	
	lda #col2
	sta color	
	lda mainvolume	
	ldy #yvolinfo
	ldx #35
	jsr print2digit					
	ldy #ystereo
	ldx #38
	lda #col6
	sta color			
	lda monomode
	lsr
	bcc !+
	lda #<strstereo
	sta arg1
	lda #>strstereo
	sta arg2
	jmp printstring
!:
	lda #<strmono
	sta arg1
	lda #>strmono
	sta arg2
	jmp printstring
	
	
// Print single channel note info
// ******************************
printplayingnote:
	:structload(bufferptr,sftflgoff)
	lsr
	lda #col2	
	bcs !+
	lda #col5
!:
	sta color
	:structload(bufferptr,sftnotoff)		
	ldy #ynoteinfo	
	jsr printnote
	:structload(bufferptr,sftsmpoff)		
	ldy #ynoteinfo
	jsr print2digit
	:structload(bufferptr,sfttfxoff)			
	ldy #ynoteinfo	
	jsr print1digit
	:structload(bufferptr,sfttxdoff)
	ldy #ynoteinfo	
	jmp print2digit
	

// Disable NMI
// ***********	
disablenmi:
        sei
        lda #<nmi
        sta $0318
        lda #>nmi
        sta $0319
        lda #$00
        sta $DD0E
        sta $DD04
        sta $DD05
        lda #$81
        sta $DD0D
        lda #$01
        sta $DD0E
		rts
		
.align $0100	
.pc=* "Tables"
// Finetunes table
// ***************
finetunel:
.import binary "Tools\finetune.bin"
.label finetuneh=finetunel+36*16

// Font
// ****
.align $0100
charsetrom:
.import binary "Fonts\8x8.c64"
.print "After font: $"+toHexString(*)

// Pro tracker sinus table
// ***********************
.var ptsine=List().add(   0, 24, 49, 74, 97,120,141,161,
						180,197,212,224,235,244,250,253,
						255,253,250,244,235,224,212,197,
						180,161,141,120, 97, 74, 49, 24)

// Waveforms used in FX section
// ****************************
.align $0080
fxwaveforms:
.print "FX waveform table: $"+toHexString(*)
.fill 32,ptsine.get(i)											// Sine
.fill 32,255-i*8												// Ramp
.fill 32,255													// Square
.fill 32,round(256*random())									// Random

// Amiga period values
// *******************
.align $0100
notelookup:
.print "Note lookup: $"+toHexString(*)
.import binary "Tools\notelookupgls.bin"
.print "Finetune (LO): $"+toHexString(finetunel)
.print "Finetune (HI): $"+toHexString(finetuneh)

// TO DO:
//
// File browser: $1300 byte available

.if (test!=true) {
	.pc=$c000
}
