// Non zeropage variables and data structures
// ******************************************

// Buffers data
.const	buffers			=	varbase	// Play buffers
.const	buffer1			=	buffers+$00
.const	buffer2			=	buffers+$40
.const	buffer3			=	buffers+$80
.const	buffer4			=	buffers+$C0

// Sample data
.const	sampleptrl		=	varbase+$0100
.const	sampleptrm		=	varbase+$0100+[maxsamplesz+1]*1
.const	sampleptrh		=	varbase+$0100+[maxsamplesz+1]*2
.const	samplelenl		=	varbase+$0100+[maxsamplesz+1]*3
.const	samplelenm		=	varbase+$0100+[maxsamplesz+1]*4
.const	samplelenh		=	varbase+$0100+[maxsamplesz+1]*5
.const	samplerepal		=	varbase+$0100+[maxsamplesz+1]*6
.const	samplerepam		=	varbase+$0100+[maxsamplesz+1]*7
.const	samplerepah		=	varbase+$0100+[maxsamplesz+1]*8
.const	samplerepbl		=	varbase+$0100+[maxsamplesz+1]*9
.const	samplerepbm		=	varbase+$0100+[maxsamplesz+1]*10
.const	samplerepbh		=	varbase+$0100+[maxsamplesz+1]*11
.const	samplerep		=	varbase+$0100+[maxsamplesz+1]*12
.const	samplevol		=	varbase+$0100+[maxsamplesz+1]*13
.const	sampleft		=	varbase+$0100+[maxsamplesz+1]*14

// Sample list
.var	samples		=	List(maxsamplesz)	// List for all instruments
.var	smpdatasize	=	30					// Sample data size

// Module structure
.const	mod_name			=	varbase+$0300		// ** Module Name **				   [20 bytes]
.const	mod_sample_header	=	varbase+$0300+20	// ** Start of header/Sample data **   [31x30 bytes]
.for(var i=0;i<maxsamplesz;i++)
	.eval samples.set(i,varbase+$0300+i*smpdatasize+20)
.const	mod_song			=	varbase+$0300+maxsamplesz*smpdatasize+20	// ** Song Data ** [134 bytes]
.const	division			=	varbase+$0780

// REU data
.const	reu_testpattern		=	codebase		// Test pattern to be sent to REU
.const	reu_savepattern		=	varbase+$0800	// Data from REU
.const	reu_comppattern		=	varbase+$0900	// Get pattern from REU to compare

// Generic data

// Keyboard routines
.const	GKJtemp		=	varbase+$0a00+$00
.const	keyrepeat	=	varbase+$0a00+$01
.const	curkey		=	varbase+$0a00+$02

// FX routines
.const	fxjump		=	varbase+$0a00+$03	//	Indirect jump for fx

// Functions
.const	p2dtemp		=	varbase+$0a00+$04

// Strings
// Strings
// *******	
.const	startstrings=	varbase+$0b00		// Strings copy
.const	strnotes	=	orgstrnotes-orgstartstrings+startstrings
.const	strdet		=	orgstrdet-orgstartstrings+startstrings
.const	strdetaud	=	orgstrdetaud-orgstartstrings+startstrings
.const	strreufail	=	orgstrreufail-orgstartstrings+startstrings
.const	strreuok	=	orgstrreuok-orgstartstrings+startstrings
.const	strreusize	=	orgstrreusize-orgstartstrings+startstrings
.const	straudver	=	orgstraudver-orgstartstrings+startstrings
.const	strname		=	orgstrname-orgstartstrings+startstrings
.const	strminus	=	orgstrminus-orgstartstrings+startstrings
.const	strsonglenght	=	orgstrsonglenght-orgstartstrings+startstrings
.const	strpatternnumber=	orgstrpatternnumber-orgstartstrings+startstrings
.const	strsongplayptn	=	orgstrsongplayptn-orgstartstrings+startstrings
.const	strsongplaydiv	=	orgstrsongplaydiv-orgstartstrings+startstrings
.const	strmodnotsupp	=	orgstrmodnotsupp-orgstartstrings+startstrings
.const	strreukb	=	orgstrreukb-orgstartstrings+startstrings
.const	strreu0		=	orgstrreu0-orgstartstrings+startstrings
.const	strreu1		=	orgstrreu1-orgstartstrings+startstrings
.const	strreu2		=	orgstrreu2-orgstartstrings+startstrings
.const	strreu3		=	orgstrreu3-orgstartstrings+startstrings
.const	strreu4		=	orgstrreu4-orgstartstrings+startstrings
.const	strreu5		=	orgstrreu5-orgstartstrings+startstrings
.const	strreu6		=	orgstrreu6-orgstartstrings+startstrings
.const	strreu7		=	orgstrreu7-orgstartstrings+startstrings
.const	strvolume	=	orgstrvolume-orgstartstrings+startstrings
.const	strchn1		=	orgstrchn1-orgstartstrings+startstrings
.const	strchn2		=	orgstrchn2-orgstartstrings+startstrings
.const	strchn3		=	orgstrchn3-orgstartstrings+startstrings
.const	strchn4		=	orgstrchn4-orgstartstrings+startstrings
.const	strmono		=	orgstrmono-orgstartstrings+startstrings
.const	strstereo	=	orgstrstereo-orgstartstrings+startstrings
.const	strtitle	=	orgstrtitle-orgstartstrings+startstrings
.const	endstrings	=	orgendstrings-orgstartstrings+startstrings

// REU sizes
// *********
strreutbll:
.byte <strreu0,<strreu1,<strreu2,<strreu3,<strreu4,<strreu5,<strreu6,<strreu7
strreutblh:
.byte >strreu0,>strreu1,>strreu2,>strreu3,>strreu4,>strreu5,>strreu6,>strreu7

