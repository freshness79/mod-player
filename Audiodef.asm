// Audio module channels
// *********************
.const audch1	=	$df20
.const audch2	=	$df40
.const audch3	=	$df60
.const audch4	=	$df80
.const audch5	=	$dfa0
.const audch6	=	$dfc0
.const audch7	=	$dfe0

// Audio module register definitions
// *********************************
// Read
.const	audist	=	$00	//  8 bit - Interrupt status
.const	audver	=	$01	//  8 bit - Version
// Write
.const	audctr	=	$00	//  8 bit - Control
.const	audvol	=	$01	//  8 bit - Volume
.const	audpan	=	$02	//  8 bit - Panning
.const	audsms	=	$04	// 32 bit - Start position
.const	audsml	=	$09	// 24 bit - Lenght
.const	audrat	=	$0e	// 16 bit - Period (rate)
.const	audrpa	=	$11	// 24 bit - Repeat point A
.const	audrpb	=	$15	// 24 bit - Repeat point B
.const	audirq	=	$1f	//  8 bit - IRQ settings