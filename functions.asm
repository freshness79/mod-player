// Miscellaneous functions
// ***********************
// Mainly print utilities

// Fix strings
// ***********
// Fix kickass string for DOS font 
fixstrings:
	lda #>orgstartstrings
	sta pointer+1
	lda #<orgstartstrings
	sta pointer
	lda #>startstrings
	sta pointer+3
	lda #<startstrings
	sta pointer+2
	ldy #$00
fsloop:
	lda (pointer),y
	cmp #$01
	bcc !+
	cmp #$1b
	bcs !+
	adc #$60
!:		
	sta (pointer+2),y
	iny
	cpy #<[$100+orgendstrings-orgstartstrings]
	beq !+
fscontinue:
	cpy #$00
	bne fsloop
	inc pointer+1
	inc pointer+3
	bne fsloop
!:
	lda pointer+1
	cmp #>orgendstrings
	bne fscontinue	
	rts

// Print 0 terminated string on x,y 
// ********************************
printstring:	// x,y, stack (hi lo)
	lda arg1
	sta pointer
	lda arg2
	sta pointer+1
	tya
	pha
	txa
	clc
	adc rowl,y
	sta pointer+2
	sta pointer+4
	lda rowh,y
	adc #$00
	sta pointer+3	
	and #$03
	ora #$d8
	sta pointer+5
	ldy #$00
psload:
	lda (pointer),y
	beq psexit
pssave:
	sta (pointer+2),y
	lda color
pcsave:	
	sta (pointer+4),y	
	iny
	inx
	cpy printlimit
	bne psload
psexit:
	pla
	tay
	rts

// Color string on x,y 
// *******************
colorstring:	// x,y, stack (hi lo)
	sta temp
	tya
	pha
	txa
	clc
	adc rowl,y
	sta pointer
	lda rowh,y
	sta pointer+1
	lda temp
	ldy #$00
cssave:
	sta (pointer),y	
	iny
	cpy printlimit
	bne cssave
	pla
	tay
	rts	

// Print 2 hex digit
// *****************
print2digit:
	sta p2dtemp
	txa
	pha	
	clc
	adc rowl,y	
	sta pointer
	sta pointer+2
	lda rowh,y
	adc #$00
	sta pointer+1
	and #$03
	ora #$d8
	sta pointer+3
	lda p2dtemp
	pha
	sty p2dtemp
	ldy #$00	
	lsr
	lsr
	lsr
	lsr
	tax
	lda hextbl,x
	sta (pointer),y
	lda color
	sta (pointer+2),y
	pla
	and #$0f
	tax
	iny
	lda hextbl,x
	sta (pointer),y
	lda color
	sta (pointer+2),y	
	pla
	tax
	inx
	inx
	ldy p2dtemp
	rts

	
// Print 1 hex digit
// *****************
print1digit:
	sta p2dtemp
	txa
	pha	
	clc
	adc rowl,y	
	sta pointer
	sta pointer+2
	lda rowh,y
	adc #$00
	sta pointer+1
	and #$03
	ora #$d8
	sta pointer+3
	lda p2dtemp	
	and #$0f
	tax	
	lda hextbl,x
	sty p2dtemp
	ldy #$00
	sta (pointer),y
	lda color
	sta (pointer+2),y
	pla
	tax
	inx	
	ldy p2dtemp
	rts
	

// Print note
// **********
printnote:
	asl
	rol temp
	asl 
	rol temp
	clc
	adc #<strnotes
	sta arg1
	lda temp
	and #$03
	adc #>strnotes
	sta arg2
	jmp printstring
	
	
// Clearscreen
// ***********
clearscreen:
	lda #$20
	ldx #$00
!:
	sta charscreen+$0000,x
	sta charscreen+$0100,x
	sta charscreen+$0200,x
	sta charscreen+$0300,x
	dex
	bne !-
	rts	
	
// Set screen
// **********
setscreen:
	lda #$1b
	sta $d011
	lda #$c8
	sta $d016
	rts
	
// Initialize screen	
// *****************
initscreen:		
	jsr clearscreen
	lda #$28
	sta printlimit
	lda #[floor([charset&$3fff]/$0400)|[[[charscreen&$3fff]/$0400]*$10]]
	sta $d018
	lda $dd00
	and #$fc
	ora #[3^[<[charset/$4000]]]
	sta $dd00
	jsr setscreen
	rts
	

// Copy charset
// ************
copycharset:
	ldx #$00
!:
	.for(var i=0;i<8;i++) {
		lda charsetrom+i*$100,x
		sta charset+i*$100,x
	}
	dex
	bne !-
	rts	

	
// Fast row table
// **************
rowl:
.fill 25,<[40*i+charscreen]
rowh:
.fill 25,>[40*i+charscreen]

// Hex lookup table
// ****************
hextbl:
.fill 10,$30+i
.fill 6,$40+i+1	// Special charset
