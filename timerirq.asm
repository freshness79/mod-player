// IRQ settings & handlers routines
// ********************************

// Constants

.const	romirq		=	$ea31
.const	fakeirq		=	$ea7e

// Set fake IRQ
// ************
setfakeirq:
	sei
	lda #$00
	sta $d01a
	lda #$7f
	sta $dd0d
	lda $dd0d
	lda #<fakeirq
	sta $0314
	lda #>fakeirq
	sta $0315
	cli
	rts
	

// Set IRQ	
// *******
setmainirq:	
	sei
	lda $dc0d
	lda #<mainirq
	sta $0314
	lda #>mainirq
	sta $0315
	cli	
	rts
	
	
// Play mode IRQ
// *************
mainirq:		
	jsr playmodule	
	jsr printdata
	jmp fakeirq


// Set standard IRQ
// ****************
setstdirq:
	sei
	lda #$81
	sta $dc0d
	lda #<romirq
	sta $0314
	lda #>romirq
	sta $0315
	cli
	rts
	
	
// Set timer
// *********
settimer:
	php
	sec
	lda timer
	sbc #$01
	sta $dc04
	lda timer+1
	sbc #$00
	sta $dc05
	lda newspeed
	sta tickperdiv
	plp
	rts	
