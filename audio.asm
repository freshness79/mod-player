// 1541 Audio expansion
// ********************

// Reset Audio Module
// ******************
resetaudio:
	lda #$00
	ldx #$1e
!:
	sta audch1,x
	sta audch2,x
	sta audch3,x
	sta audch4,x
	sta audch5,x
	sta audch6,x
	sta audch7,x
	dex
	bpl !-
	rts

// Detect 1541U2 audio module
// **************************
// OUT: C=0 (not detected)
//      C=1 A=version
detectaudio:
	sei
	lda #$00		// Stop DACs
	sta audch1+audctr
	sta audch2+audctr
	sta audch3+audctr
	sta audch4+audctr
	sta audch5+audctr
	sta audch6+audctr
	sta audch7+audctr
	lda #$ff
	sta audch1+audirq		// Ack IRQs
	ldx #$00
!:
	lda audch1+audist		// IRQ status test (should be 0 - Test it for 256 times!)
	bne audnotfound
	dex
	bne !-
	lda #$00		// Start fake sample
	sta audch1+audvol	
	lda #$00
	sta audch1+audsms
	sta audch1+audsms+1
	sta audch1+audsms+2
	sta audch1+audsms+3
	sta audch1+audrat
	sta audch1+audsml
	lda #$01
	sta audch1+audrat+1
	sta audch1+audsml+1	
	lda #%00000101
	sta audch1+audctr	
	ldx #$80		
!:
	lda audch1+audist	// Wait for IRQ...
	bne !+
	dex
	bne !-
	ldx #$00
!:
	lda audch1+audist	// Test IRQ flags (should be 1 - Test it for 256 times!)
	cmp #$01	
	bne audnotfound
	dex 
	bne !-
	lda #$ff
	sta audch1+audirq
	lda audch1+audver
	sec
	cli	
	rts	
audnotfound:
	clc
	cli
	rts

		
// Fast play routine
// *****************
// Play 4 channels by reading buffered data
fastplay:

	// Fast play - Voice 1 -

	lda buffer1+sftcmdoff
	lsr
	bcc !+		// Check volume
	ldx buffer1+audvol
	stx audch1+audvol
!:
	lsr
	bcc !+		// Check panning
	ldx buffer1+audpan
	stx audch1+audpan
!:	
	lsr
	bcc !+		// Check sample rate
	ldx buffer1+audrat
	stx audch1+audrat
	ldx buffer1+audrat+1
	stx audch1+audrat+1
!:
	lsr
	bcc !+		// Check repeat
	beq !+
	ldx buffer1+audrpa
	stx audch1+audrpa
	ldx buffer1+audrpa+1
	stx audch1+audrpa+1
	ldx buffer1+audrpa+2
	stx audch1+audrpa+2
	ldx buffer1+audrpb
	stx audch1+audrpb
	ldx buffer1+audrpb+1
	stx audch1+audrpb+1
	ldx buffer1+audrpb+2
	stx audch1+audrpb+2	
!:	
	lsr
	bcc !+		// Check stop/play
	ldx buffer1+audsms
	stx audch1+audsms	
	ldx buffer1+audsms+1
	stx audch1+audsms+1	
	ldx buffer1+audsms+2
	stx audch1+audsms+2	
	ldx buffer1+audsms+3
	stx audch1+audsms+3	
	ldx buffer1+audsml
	stx audch1+audsml
	ldx buffer1+audsml+1
	stx audch1+audsml+1
	ldx buffer1+audsml+2
	stx audch1+audsml+2		
	ldx #$00
	stx audch1+audctr
	ldx buffer1+audctr
	stx audch1+audctr
!:

	// Fast play - Voice 2 -

	lda buffer2+sftcmdoff
	lsr
	bcc !+		// Check volume
	ldx buffer2+audvol
	stx audch2+audvol
!:
	lsr
	bcc !+		// Check panning
	ldx buffer2+audpan
	stx audch2+audpan
!:	
	lsr
	bcc !+		// Check sample rate
	ldx buffer2+audrat
	stx audch2+audrat
	ldx buffer2+audrat+1
	stx audch2+audrat+1
!:
	lsr
	bcc !+		// Check repeat
	beq !+
	ldx buffer2+audrpa
	stx audch2+audrpa
	ldx buffer2+audrpa+1
	stx audch2+audrpa+1
	ldx buffer2+audrpa+2
	stx audch2+audrpa+2
	ldx buffer2+audrpb
	stx audch2+audrpb
	ldx buffer2+audrpb+1
	stx audch2+audrpb+1
	ldx buffer2+audrpb+2
	stx audch2+audrpb+2	
!:	
	lsr
	bcc !+		// Check stop/play
	ldx buffer2+audsms
	stx audch2+audsms
	ldx buffer2+audsms+1
	stx audch2+audsms+1
	ldx buffer2+audsms+2
	stx audch2+audsms+2
	ldx buffer2+audsms+3
	stx audch2+audsms+3
	ldx buffer2+audsml
	stx audch2+audsml
	ldx buffer2+audsml+1
	stx audch2+audsml+1
	ldx buffer2+audsml+2
	stx audch2+audsml+2	
	ldx #$00
	stx audch2+audctr
	ldx buffer2+audctr
	stx audch2+audctr
!:

	// Fast play - Voice 3 -

	lda buffer3+sftcmdoff
	lsr
	bcc !+		// Check volume
	ldx buffer3+audvol
	stx audch3+audvol
!:
	lsr
	bcc !+		// Check panning
	ldx buffer3+audpan
	stx audch3+audpan
!:	
	lsr
	bcc !+		// Check sample rate
	ldx buffer3+audrat
	stx audch3+audrat
	ldx buffer3+audrat+1
	stx audch3+audrat+1
!:
	lsr
	bcc !+		// Check repeat
	beq !+
	ldx buffer3+audrpa
	stx audch3+audrpa
	ldx buffer3+audrpa+1
	stx audch3+audrpa+1
	ldx buffer3+audrpa+2
	stx audch3+audrpa+2
	ldx buffer3+audrpb
	stx audch3+audrpb
	ldx buffer3+audrpb+1
	stx audch3+audrpb+1
	ldx buffer3+audrpb+2
	stx audch3+audrpb+2	
!:	
	lsr
	bcc !+		// Check stop/play
	ldx buffer3+audsms
	stx audch3+audsms
	ldx buffer3+audsms+1
	stx audch3+audsms+1
	ldx buffer3+audsms+2
	stx audch3+audsms+2
	ldx buffer3+audsms+3
	stx audch3+audsms+3
	ldx buffer3+audsml
	stx audch3+audsml
	ldx buffer3+audsml+1
	stx audch3+audsml+1
	ldx buffer3+audsml+2
	stx audch3+audsml+2	
	ldx #$00
	stx audch3+audctr
	ldx buffer3+audctr
	stx audch3+audctr
!:

	// Fast play - Voice 4 -

	lda buffer4+sftcmdoff
	lsr
	bcc !+		// Check volume
	ldx buffer4+audvol
	stx audch4+audvol
!:
	lsr
	bcc !+		// Check panning
	ldx buffer4+audpan
	stx audch4+audpan
!:	
	lsr
	bcc !+		// Check sample rate
	ldx buffer4+audrat
	stx audch4+audrat
	ldx buffer4+audrat+1
	stx audch4+audrat+1
!:	
	lsr
	bcc !+		// Check repeat
	beq !+
	ldx buffer4+audrpa
	stx audch4+audrpa
	ldx buffer4+audrpa+1
	stx audch4+audrpa+1
	ldx buffer4+audrpa+2
	stx audch4+audrpa+2
	ldx buffer4+audrpb
	stx audch4+audrpb
	ldx buffer4+audrpb+1
	stx audch4+audrpb+1
	ldx buffer4+audrpb+2
	stx audch4+audrpb+2	
!:	
	lsr
	bcc !+		// Check stop/play
	ldx buffer4+audsms
	stx audch4+audsms
	ldx buffer4+audsms+1
	stx audch4+audsms+1
	ldx buffer4+audsms+2
	stx audch4+audsms+2
	ldx buffer4+audsms+3
	stx audch4+audsms+3
	ldx buffer4+audsml
	stx audch4+audsml
	ldx buffer4+audsml+1
	stx audch4+audsml+1
	ldx buffer4+audsml+2
	stx audch4+audsml+2	
	ldx #$00
	stx audch4+audctr
	ldx buffer4+audctr
	stx audch4+audctr
!:	
	lda #$00
	sta buffer1+sftcmdoff
	sta buffer2+sftcmdoff
	sta buffer3+sftcmdoff
	sta buffer4+sftcmdoff
	rts
	
// Volume setting (Bypass player)
// ****************************** 	
setvol:
	pha	
	lda audchnoff,x	
	ora #audvol
	tax	
	pla
	sta audch1,x
	rts

	
// Panning setting (Bypass player)
// *******************************
setpan:
	pha	
	lda audchnoff,x	
	ora #audpan
	tax		
	pla	
	sta audch1,x
	rts
	
audchnoff:
.byte $00,$20,$40,$60,$80,$a0,$c0