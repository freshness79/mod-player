// Audio module channel configuration offsets
// ******************************************
// First $20 register are an exact copy of audio module registers

// Player channel configuration offsets
// ************************************
.const	sftcmdoff	=	$20	//  8 bit - Command flags (See below)
.const	sftsmpoff	=	$21	//  8 bit - Sample number+1
.const	sfttfxoff	=	$22	//  8 bit - FX number
.const	sfttxdoff	=	$23	//  8 bit - FX data
.const	sftnrtoff	=	$24	// 16 bit - Original rate (LO/HI)
.const	sftortoff	=	$26	// 16 bit - Last rate (LO/HI)
.const	sftnotoff	=	$28	//	8 bit - Played note (may be 0)
.const	sftlntoff	=	$29	//	8 bit - Last note played (should not be 0)
.const	sftnntoff	=	$2a	//  8 bit - Note played in last line (flag)
//.const	?		=	$2b	//  8 bit - 
.const	sftvbaoff	=	$2c	//  8 bit - Vibrato amplitude
.const	sftvbsoff	=	$2d	//  8 bit - Vibrato speed
.const	sftvbpoff	=	$2e	//  8 bit - Vibrato position
.const	sftvbwoff	=	$2f	//  8 bit - Vibrato waveform
.const	sfttraoff	=	$30	//  8 bit - Tremolo amplitude
.const	sfttrsoff	=	$31	//  8 bit - Tremolo speed
.const	sfttrpoff	=	$32	//  8 bit - Tremolo position
.const	sfttrwoff	=	$33	//  8 bit - Tremolo waveform
.const	sftlppoff	=	$34	//  8 bit - Loop position
.const	sftlpcoff	=	$35	//  8 bit - Loop counter
.const	sfttkcoff	=	$36	//  8 bit - Tick counter
.const	sfttksoff	=	$37	//  8 bit - Tick stop
.const	sftbkcoff	=	$38	//  8 bit - Backup command for delayed play
.const	sftstsoff	=	$39	// 	8 bit - Slide to note speed
.const	sftarcoff	=	$3a	//	8 bit - Arpeggio counter
.const	sftvoloff	=	$3b	//	8 bit - Pre adjusted volume
.const	sftlvloff	=	$3c	//	8 bit - Last volume (Needed fot tremolo)
.const	sftpanoff	=	$3d	//	8 bit - Panning position
.const	sftvolsld	=	$3e	//	8 bit - Volume slide
.const	sftflgoff	=	$3f //  8 bit - bit 0: Active voice / bit 1: glissando mode

//.const	sftglsoff	=	xx	// 16 bit - Glissando rate

// Command flags
// *************


.const	flagvolume	=	1	// Volume update
.const	flagpanning	=	2	// Panning update
.const	flagrate	=	4	// Rate update 
.const	flagrepeat	=	8	// Repeat update
.const	flagnewsmp	=	16	// New sample
.const	flagvibrato	=	32	// Special vibrato flag
