// REU definitions
// ***************
// reu_detect: detect (carry) and possibly get (Accumulator) REU size.

.const	reu_status	=	$df00
.const	reu_command	=	$df01
.const	reu_c64base	=	$df02
.const	reu_reubase	=	$df04
.const	reu_translen=	$df07
.const	reu_irqmask	=	$df09
.const	reu_control	=	$df0a

// REU commands
// ************

.const	reucmp64	=	$90|$03
.const	reutoc64	=	$90|$01
.const	c64toreu	=	$90|$00

// Routines
// ********

// Test REU routine
// ****************
// Used by reu_detect routine
testreurtn:
	lda #$00
	sta reu_control	
	lda #$02
	sta reu_translen
	lda #$00
	sta reu_translen+1	
	lda #$00
	sta reu_reubase	
	sta reu_reubase+1	
	sty reu_reubase+2
	lda temp
	sta reu_c64base+1		
	sty reu_c64base
	stx reu_command
	dey
	dey
	cpy #$fe
	bne testreurtn
	rts

	
// Detect REU
// *************
// OUT: C (1 if bank exists) 
//      A (Number of 128k banks)
reu_detect:
	lda #>reu_savepattern
	sta temp
	ldy #$fe
	ldx #reutoc64			// Save REU bytes
	jsr testreurtn	
	lda #>reu_testpattern
	sta temp
	ldx #c64toreu			// Put bytes on REU
	jsr testreurtn
	lda #>reu_comppattern
	sta temp
	ldx #reutoc64
	jsr testreurtn			// Get bytes from REU
	lda #>reu_savepattern
	sta temp
	ldx #c64toreu			
	jsr testreurtn			// Restore bytes on REU		
	ldy #$00				// Compare bytes
!:
	lda reu_testpattern,y
	cmp reu_comppattern,y
	bne !+
	iny	
	bne !-
	jmp reu_16mb
!:	
	tya
	lsr
	sec
	bne !+
	clc
!:			
	rts
reu_16mb:
	lda #$80
	sec
	rts

// Reu load 16 bytes
// *****************
// IN: A   - Lenght - MAX 255(*16 bytes)
//     X,Y - C64 address
//     arg1,arg2,arg3: REU address
reu_load16:	
	asl
	rol temp
	asl
	rol temp	
	asl
	rol temp	
	asl
	rol temp		
	sta reu_translen
	lda temp
	and #$0f	
	sta reu_translen+1		
	lda #$00
	sta reu_control
	lda arg1
	sta reu_reubase
	lda arg2
	sta reu_reubase+1	
	lda arg3
	sta reu_reubase+2
	sty reu_c64base
	stx reu_c64base+1
	lda #reutoc64
	sta reu_command
	rts
	
// Reu load 16 bytes (Fixed lenght)
// ********************************
// IN: Y/Acc/X - REU address
reu_loadfix16:		
	stx reu_reubase	
	sty reu_reubase+1		
	sta reu_reubase+2	
	lda #$10
	sta reu_translen
	lda #$00
	sta reu_translen+1
	lda #<division
	sta reu_c64base
	lda #>division
	sta reu_c64base+1
	lda #$00
	sta reu_control	
	lda #reutoc64
	sta reu_command
	rts	
	
// Reu compare (fixed lenght)
// **************************
// IN: Y/Acc/X - REU address
// Used to detect new songs
reu_cmpfix20:		
	lda #$00
	sta reu_reubase	
	sta reu_reubase+1		
	sta reu_reubase+2	
	lda #$14
	sta reu_translen
	lda #$00
	sta reu_translen+1	
	stx reu_c64base	
	sty reu_c64base+1
	lda #$00
	sta reu_control	
	lda #reucmp64
	sta reu_command
	lda reu_status
	and #$20
	rts		
