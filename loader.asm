// Load module
// ***********
loadmodule:
	ldx #>mod_name	// Load module name
	ldy #<mod_name	
	lda #$00
	sta arg1
	sta arg2
	sta arg3
	lda #[floor([namesz+maxsamplesz*singlesz+songsz]/16)+1]
	jsr reu_load16
	rts

	
// Detect MOD
// **********
// OUT: C=1 Mod supported
//      C=0 Mod not supported
detectmod:
	lda #<mod_name+namesz+samplesz31+songsz
	sta pointer
	lda #>mod_name+namesz+samplesz31+songsz
	sta pointer+1
	ldy #$03
!:
	lda (pointer),y
	cmp format1,y
	bne !+
	dey
	bpl !-
	jmp dm31
!:
	ldy #$03
!:
	lda (pointer),y
	cmp format2,y
	bne !+
	dey
	bpl !-
	jmp dm31
!:
	ldy #$03
!:
	lda (pointer),y
	cmp format3,y
	bne !+
	dey
	bpl !-
	jmp dm31	
!:
	ldy #$03
!:
	lda (pointer),y
	cmp format4,y
	bne !+
	dey
	bpl !-
	jmp dm31
!:
	ldy #$03
!:
	lda (pointer),y
	cmp #$20
	bcc dm15
	cmp #$7f
	bcs dm15
	dey
	bpl !-	
!:
	clc				// Not found
	rts
	
dm31:
	lda #31
	sta nsamples
	lda #$01
	sta tag
	sec
	rts

dm15:
	lda #15
	sta nsamples
	lda #$00
	sta tag
	sec
	rts	

// Supported modes
// ***************
format1:
.text "FLT4"
format2:
.text "M.K."
format3:
.text "M!K!"
format4:
.text "4CHN"


// Prepare data for playing
// ************************
setmodstructures:			
	ldy #singlesz			// nsamples*singlesz	
	lda nsamples			// Calc samples header size
	jsr mul8x8
		
	clc						// songptr = namesz + nsamples*singlesz
	lda #<[namesz]	
	adc PRODUCT	
	sta songptr
	lda #>[namesz]
	adc PRODUCT+1
	sta songptr+1
	
	clc						// songptr = namesz + nsamples*singlesz
	lda songptr
	adc #<[mod_name]
	sta songptr
	lda songptr+1
	adc #>[mod_name]
	sta songptr+1	

	:structload(songptr,0)
	sta songlenght	

	clc						// songptr +=2
	lda songptr
	adc #$02
	sta songptr
	lda songptr+1
	adc #$00
	sta songptr+1	
	
	clc						// patternptr = namesz + nsamples*singlesz+songsz
	lda PRODUCT
	adc #<[songsz+namesz]
	sta patternptr
	lda PRODUCT+1
	adc #>[songsz+namesz]
	sta patternptr+1		
	
	lda tag					// Add tag if needed (Fix for 15 instruments mods)
	asl
	asl
	adc patternptr
	sta patternptr
	lda patternptr+1
	adc #0
	sta patternptr+1		
		
	ldy #$7f
	lda #$00
getmaxpattern:
	cmp (songptr),y
	bcs !+
	lda (songptr),y
!:
	dey
	bpl	getmaxpattern
	sta patternsnumber	// MAX pattern = PatternNumber-1
	iny
	lda (songptr),y
	sta curpattern
	
	inc patternsnumber	// => Pattern number
	
	lda #$00				// sampleptr = PatternNumber*16*64 = PN*$400
	sta sampleptrl
	sta sampleptrh
	clc
	lda patternsnumber	
	asl
	rol sampleptrh
	asl
	rol sampleptrh
	sta sampleptrm
			
	clc						// sampleptr = Patternnumber*$400+ namesz + songsz
	lda sampleptrl
	adc patternptr
	sta sampleptrl
	lda sampleptrm
	adc patternptr+1
	sta sampleptrm
	lda sampleptrh
	adc #$00
	sta sampleptrh	
		
	ldx #$00
readsampleloop:
	lda samplelistl,x
	sta pointer	
	lda samplelisth,x
	sta pointer+1		
	
	:structload(pointer,23)	// Compute lenght*2
	asl
	sta samplelenl,x
	:structload(pointer,22)
	rol
	sta samplelenm,x
	lda #$00
	rol
	sta samplelenh,x
	
	:structload(pointer,24)	// Save Finetune value
	and #$0f				// Just to be safe...
	sta sampleft,x
	
	lda sampleptrl,x		// Compute starting position of next sample
	adc samplelenl,x	
	sta sampleptrl+1,x
	lda sampleptrm,x
	adc samplelenm,x	
	sta sampleptrm+1,x
	lda sampleptrh,x
	adc samplelenh,x
	sta sampleptrh+1,x
			
	:structload(pointer,25)	// Compute volume	
	ldy #$ff
	jsr mul8x8
	lda PRODUCT+1
	sta samplevol,x

	:structload(pointer,29)	// Compute rep flag		
	cmp #$02				// Only loop if size >1
	bcs !+	
	:structload(pointer,28)
	bne !+
	beq skiprep
!:	
	inc samplerep,x
	
	:structload(pointer,27)	// Compute repeat A
	cmp #$01
	bcc !+
    sbc #$01				// If possible remove 2 bytes (value 1)
!:
	asl
	sta samplerepal,x
	:structload(pointer,26)
	rol
	sta samplerepam,x
	lda #$00
	rol
	sta samplerepah,x
	
	:structload(pointer,29)	// Compute repeat B
	asl
	sta temp
	:structload(pointer,28)
	rol
	sta temp2
	lda #$00
	rol 
	sta temp3	

	lda samplerepal,x
	adc temp
	sta samplerepbl,x	
	lda samplerepam,x
	adc temp2
	sta samplerepbm,x	
	lda samplerepah,x
	adc temp3
	sta samplerepbh,x

.pc=* "TEST"	
	cmp samplelenh,x	// Looks like some MODS exceeds sample length, they need this fix
	bcc skiprep
	bne fixrep
	lda samplerepbm,x
	cmp samplelenm,x
	bcc skiprep
	bne fixrep
	lda samplerepbl,x
	cmp samplelenl,x
	bcc skiprep
fixrep:	
	lda samplelenl,x
	sta samplerepbl,x
	lda samplelenm,x
	sta samplerepbm,x
	lda samplelenh,x
	sta samplerepbh,x
	
skiprep:
	inx
	cpx nsamples	
	beq !+
	jmp	readsampleloop
!:	
	rts
	
	
// Reset sample info
// *****************
clearsampleinfo:
	ldx #maxsamplesz
	lda #$00
!:
	sta sampleptrl,x
	sta sampleptrm,x
	sta sampleptrh,x
	sta samplelenl,x
	sta samplelenm,x
	sta samplelenh,x
	sta	samplerepal,x
	sta	samplerepam,x
	sta	samplerepah,x
	sta	samplerepbl,x
	sta	samplerepbm,x
	sta	samplerepbh,x	
	sta samplerep,x
	sta samplevol,x
	sta	sampleft,x	
	dex 
	bpl !-
	rts
	

// Clear module data
// *****************
clearmoduledata:
	ldx #20-1
	lda #$00
!:
	sta mod_name,x
	dex
	bpl !-
	ldx #30-1
!:
	.for(var i=0;i<maxsamplesz;i++)
		sta mod_sample_header+smpdatasize*i,x
	dex
	bpl !-
	ldx #134
!:
	sta mod_song-1,x
	dex
	bne !-
	ldx #16
!:
	sta division-1,x
	dex
	bne !-	
	rts
	