// *******************
// Main FX sub routine
// *******************

.print "Tick FX case: $"+toHexString(*)
tickfx:	
	lda #<buffer1
	sta bufferptr
	jsr channelfx
	lda #<buffer2
	sta bufferptr
	jsr channelfx
	lda #<buffer3
	sta bufferptr
	jsr channelfx
	lda #<buffer4
	sta bufferptr
	jmp channelfx
	

// Parse single FX
// ***************
channelfx:	
	:structload(bufferptr,sfttfxoff)	// FX number
	cmp #$0e
	bne !+
	:structload(bufferptr,sfttxdoff)	// Special case "E"
	tax
	and #$0f
	:structsave(bufferptr,sfttxdoff)
	txa
	lsr
	lsr
	lsr
	lsr
	ora #$10
	:structsave(bufferptr,sfttfxoff)
!:	
	asl	
	adc #<fxtablet0					// First tick jump table	
	ldx curtick
	cpx tickperdiv
	beq !+		
	adc #<[fxtabletn-fxtablet0]		// Tick n jump table
!:
	sta fxjump+1
	:structload(bufferptr,sfttxdoff)	// FX data
	jsr fxjump	
	lda #$00
	:structsave(bufferptr,sftnntoff)	// Reset note played flag
	rts	


// ****************
// ** FX SECTION **
// ****************

	
// FX not implemented
// ******************	
fxnotimplemented:
	rts

	
// 00 - Arpeggio
// *************
// Tick 0 & n
fx0arpeggio:							// Tick 0	
	beq noarp							// Arpeggio?
arpstart:	
	ldx #$03							// Set counter	
	:structload(bufferptr,sftlntoff)
	
arpdo:	
	sta temp	
	txa
	:structsave(bufferptr,sftarcoff)	// Update counter	
	:structloadax(bufferptr,sftsmpoff)
	dex
	lda sampleft,x						// A = Finetune
	ldx temp							// X = note
	jsr getrate							// Get rate from note & finetune
	:structsave(bufferptr,sftnrtoff+1)	// 
	txa
	:structsave(bufferptr,sftnrtoff)	// Save new rate value	
	:structload(bufferptr,sftcmdoff)	// Enable ** SAMPLE RATE **	
	ora #flagrate
	:structsave(bufferptr,sftcmdoff)
	rts
	
noarp:
	:structload(bufferptr,sftarcoff)	// Last arpeggio still active? if so reset!
	cmp #$02	
	lda #$00
	:structsave(bufferptr,sftarcoff)	// Reset counter	
	bcs arprate
	rts

fxnarpeggio:	// Tick n
	:structloadax(bufferptr,sftarcoff)	// Get counter
	bne !+
	rts
!:	
	:structload(bufferptr,sfttxdoff)	// Get data
	cpx #$03
	bne !+
	lsr									// Play displacement 1
	lsr
	lsr
	lsr
!:
	cpx #$02
	bne !+
	and #$0f							// Play displacement 2
!:
	cpx #$01
	bne !+
	lda #$00							// No displacement
!:
	dex
	beq arpstart
arprate:
	clc	
	:structadc(bufferptr,sftlntoff)		// Add note offset
	jmp arpdo
	
	
// 01 - Slide UP
// *************
// Tick n only
fxnslideup:		// Tick n
	tay
	lda #>minslideperiod
	ldx #<minslideperiod
	jmp slidetomin

	
// 02 - Slide DOWN 
// ***************
// Tick n only	
fxnslidedown:	// Tick n
	tay
	lda #>maxslideperiod
	ldx #<maxslideperiod
	jmp slidetomax		

	
// 03 - Slide to note	
// ******************
// Tick 0 & n
fx0slidetonote:	// Tick 0
	beq fx0slidetonotenoup				// Update speed only if !=0
	:structsave(bufferptr,sftstsoff)
fx0slidetonotenoup:
	:structload(bufferptr,sftnntoff)	// Check if note played
	beq !+
	:structload(bufferptr,sftnrtoff)	// If so exchange last note with current
	tax
	:structload(bufferptr,sftortoff)
	:structsave(bufferptr,sftnrtoff)
	//:structsave(bufferptr,sftglsoff)	// Missing Glissando!
	txa
	:structsave(bufferptr,sftortoff)
	:structload(bufferptr,sftnrtoff+1)
	tax
	:structload(bufferptr,sftortoff+1)
	:structsave(bufferptr,sftnrtoff+1)
	//:structsave(bufferptr,sftglsoff+1)	// Missing Glissando!
	txa
	:structsave(bufferptr,sftortoff+1)
!:	
	:structload(bufferptr,sftcmdoff)	// Don't start new sample
	and #[$ff-flagnewsmp]
	:structsave(bufferptr,sftcmdoff)	
	rts
	
fxnslidetonote:	// Tick n		
	:structload(bufferptr,sftflgoff)	// Glissando mode?
	and #$02
	sec
	bne glissandofx		
normalfx03:	
	:structload(bufferptr,sftnrtoff)	// Old
	:structsbc(bufferptr,sftortoff)		// New
	sta temp3
	:structload(bufferptr,sftnrtoff+1)	// Old
	:structsbc(bufferptr,sftortoff+1)	// New
	ora temp3
	bne	fxstndoit
	rts
fxstndoit:
	:structload(bufferptr,sftstsoff)
	sta temp
	:structloadax(bufferptr,sftortoff)	
	:structload(bufferptr,sftortoff+1)
	ldy temp
	bcc	!+
	jmp slidetomin
!:
	jmp slidetomax

glissandofx:		
	/* :structload(bufferptr,sftglsoff)	// Check direction
	:structsbc(bufferptr,sftortoff)		// ...
	sta temp3
	:structload(bufferptr,sftglsoff+1)	// ...
	:structsbc(bufferptr,sftortoff+1)	// ...
	ora temp3
	bne	!+
	rts									// Target reached
!:
	php
	jsr xchgglissando					// Exchange sample rates
	jsr fxstndoit						// Update rate
	jsr xchgglissando					// Re-Exchange sample rates
	:structloadax(bufferptr,sftglsoff)	
	:structload(bufferptr,sftglsoff+1)		
	jsr getnotegls						// Find notes or value-to-notes given the rate	
	bmi !+	
	plp
	:structload(bufferptr,sftglsoff)	// Ok, we are on a note
	:structsave(bufferptr,sftnrtoff)
	:structload(bufferptr,sftglsoff+1)
	:structsave(bufferptr,sftnrtoff+1)	
	rts
!:
	and #$7f
	clc
	:structadc(bufferptr,sftglsoff)	
	tax
	:structload(bufferptr,sftglsoff+1)
	adc #$00
	:structsave(bufferptr,sftglsoff+1)
	jsr getnote
	plp
	adc #$00
	tax
	:structload(bufferptr,sftsmpoff)	// Get sample number...
	tay	
	lda sampleft-1,y					// ...for finetune => A
	jsr getrate							// Get rate from note	
	:structsave(bufferptr,sftnrtoff+1)	// 
	txa
	:structsave(bufferptr,sftnrtoff)	// Old
	*/
	rts
	

// 04 - Vibrato
// ************
// Tick 0 & n
fx0vibrato:		// Tick 0
	pha
	and #$0f
	beq !+								// Update amplitude if !=0
	:structsave(bufferptr,sftvbaoff)
!:	
	pla
	lsr
	lsr
	lsr
	lsr	
	beq checkvibretr					// Update speed if !=0
	:structsave(bufferptr,sftvbsoff)
checkvibretr:
	:structload(bufferptr,sftnntoff)	// Check if note played
	beq !+
	:structload(bufferptr,sftvbwoff)	// Get waveform
	cmp #$04
	bcs !+
	lda #$00
	:structsave(bufferptr,sftvbpoff)
!:
	rts

fxnvibrato:		// Tick n
	:structload(bufferptr,sftvbwoff)	// Get table pointer from waveform
	lsr
	ror
	ror	
	arr #$c0							
	sta temp							// temp = (vbwoff<<5) & 01100000
	:structload(bufferptr,sftvbpoff)	// Add position
	and #$1f
	ora temp
	tax									// <pointer> -> delta
	:structload(bufferptr,sftvbpoff)	// Update vibrato position (Carry=0 because ARR)	
	sta temp
	:structadc(bufferptr,sftvbsoff)
	cmp #$40
	bcc !+								// Back to -31 if overflow
	sbc #$40
!:	
	:structsave(bufferptr,sftvbpoff)	// OK -> Updated
	:structload(bufferptr,sftvbaoff)	// Load amplitude
	tay
	lda fxwaveforms,x					// Load table value		
	jsr mul8x8							// Multiply	
	asl PRODUCT
	rol 								// ACC = (amp*[value])>>7	
	ldx #$20
	cpx temp							// Add or subtract value based on current position	
	bcs	!+		
	:structadc(bufferptr,sftnrtoff)		// Add value
	tax
	:structload(bufferptr,sftnrtoff+1)		
	adc #$00
	jmp tfvset
!:
	sta temp							// Subtract value	
	:structload(bufferptr,sftnrtoff)	
	sbc temp
	tax
	:structload(bufferptr,sftnrtoff+1)
	sbc #$00	
tfvset:	
	tay	
	jsr fixperiod						// Translate amiga period to audio module period
	lda PRODUCT+2
	:structsave(bufferptr,audrat+1)	
	lda PRODUCT+3
	:structsave(bufferptr,audrat)		
	:structload(bufferptr,sftcmdoff)
	ora #flagvibrato					// Special case for vibrato (skip freq fixing)
	:structsave(bufferptr,sftcmdoff)		
	rts	
	
// 05 - Slide to note + Volume slide
// *********************************
// Tick 0 & n	
fx0notvolslide:		// Tick 0
	beq !+
	jsr fx0volumeslide	
!:
	jmp fx0slidetonotenoup
	
fxnnotvolslide:		// Tick n	
	beq !+
	jsr fxnvolumeslide	
!:
	jmp fxnslidetonote


// 06 - Vibrato + Volume slide
// ***************************
// Tick 0 & n		
fx0volslidevib:		// Tick 0
	beq !+
	jsr fx0volumeslide
!:
	jmp checkvibretr
	
fxnvolslidevib:		// Tick n	
	beq !+
	jsr fxnvolumeslide	
!:
	jmp fxnvibrato

	
// 07 - Tremolo
// ************
// Tick 0 & n
fx0tremolo:			// Tick 0
	pha
	and #$0f
	beq !+	
	:structsave(bufferptr,sfttraoff)
!:	
	pla
	lsr
	lsr
	lsr
	lsr	
	beq !+	
	:structsave(bufferptr,sfttrsoff)
!:		
	:structload(bufferptr,sftnntoff)	// Check if note played
	beq !+
	:structload(bufferptr,sfttrwoff)	// Get waveform
	cmp #$04
	bcs !+
	lda #$00
	:structsave(bufferptr,sfttrpoff)	
!:	
	rts

fxntremolo:		// Tick n	
	:structload(bufferptr,sfttrwoff)	// Get waveform
	lsr
	ror
	ror	
	arr #$c0							
	sta temp							// temp = (trwoff<<5) & 01100000
	:structora(bufferptr,sfttrpoff)		// Add position
	and #$1f
	ora temp
	tax									// <pointer> -> delta
	:structload(bufferptr,sfttrpoff)	// Update tremolo position (Carry=0 because ARR)	
	sta temp
	:structadc(bufferptr,sfttrsoff)
	cmp #$40
	bcc !+								// Back to -31 if overflow
	sbc #$40
!:	
	:structsave(bufferptr,sfttrpoff)	// OK -> Updated	
	:structload(bufferptr,sfttraoff)	// Load amplitude
	tay
	lda fxwaveforms,x					// Load table value		
	jsr mul8x8							// Multiply	
	asl PRODUCT
	rol 								//
	asl PRODUCT
	rol 								// ACC = (amp*[value])>>6
	ldx #$20
	cpx temp							// Add or subtract value based on current position	
	bcs	!+		
	:structadc(bufferptr,sftlvloff)		// Add value
	jmp tfvsetvol
!:
	sta temp							// Subtract value	
	:structload(bufferptr,sftlvloff)
	sbc temp
tfvsetvol:
	bpl !+
	lda #$00
!:
	cmp #$41
	bcc !+
	lda #$40
!:
	:structsave(bufferptr,sftvoloff)
	:structload(bufferptr,sftcmdoff)
	ora #flagvolume
	:structsave(bufferptr,sftcmdoff)
	rts	
	

// 08 - Panning
// ************
// Tick 0	
fx0setpanning:		// Tick 0
	lsr
	lsr
	lsr
	lsr
	:structsave(bufferptr,audpan)
	:structload(bufferptr,sftcmdoff)
	ora #flagpanning
	:structsave(bufferptr,sftcmdoff)
	rts

	
// 09 - Sample offset
// ******************
// Tick 0
fx0sampleoffset:	// Tick 0	
	sta temp
	sec
	:structload(bufferptr,audsml+1)
	sbc temp
	tax
	:structload(bufferptr,audsml)
	sbc #$00
	bmi !+		// Value over sample lenght
	:structsave(bufferptr,audsml)
	txa
	:structsave(bufferptr,audsml+1)
	clc
	:structload(bufferptr,audsms+2)
	adc temp
	:structsave(bufferptr,audsms+2)	
	:structload(bufferptr,audsms+1)
	adc #$00
	:structsave(bufferptr,audsms+1)		
	sec
	:structload(bufferptr,audrpa+1)
	sbc temp
	:structsave(bufferptr,audrpa+1)
	:structload(bufferptr,audrpa)
	sbc #$00
	:structsave(bufferptr,audrpa)
	sec
	:structload(bufferptr,audrpb+1)
	sbc temp
	:structsave(bufferptr,audrpb+1)
	:structload(bufferptr,audrpb)
	sbc #$00
	:structsave(bufferptr,audrpb)
	:structload(bufferptr,sftcmdoff)
	ora #flagnewsmp
	:structsave(bufferptr,sftcmdoff)	
!:
	rts

	
// 0A - Volume slide
// *****************
// Tick 0 & n	
fx0volumeslide:		// Tick 0
	bit hinib
	bne fxvsinc
	anc #$0f
	eor #$ff
	adc #$01
	bne fxvssave
	rts	
fxvsinc:	
	lsr
	lsr
	lsr
	lsr	
fxvssave:	
	:structsave(bufferptr,sftvolsld)
	rts
	
fxnvolumeslide:		// Tick n	
	:structload(bufferptr,sftvolsld)
fxnvolumeslideexx:	// Used by EAx & EBx
	clc
	:structadc(bufferptr,sftvoloff)	
	bpl !+
	lda #$00
!:
	cmp #$41
	bcc !+
	lda #$40
!:	
	:structsave(bufferptr,sftvoloff)	
	:structsave(bufferptr,sftlvloff)		
	:structload(bufferptr,sftcmdoff)
	ora #flagvolume
	:structsave(bufferptr,sftcmdoff)
	rts
	

// 0B - Jump pattern
// *****************
// Tick 0
fx0jumppattern:		// Tick 0
	tay
	jsr setpattern	
	sta skipdivision
	lda #$00
	sta curdivision
	lda #$01
	sta skipdivision	
	rts


// 0C - Set volume
// ***************
// Tick 0	
fx0setvolume:		// Tick 0
	:structsave(bufferptr,sftvoloff)
	:structsave(bufferptr,sftlvloff)
	:structload(bufferptr,sftcmdoff)
	ora #flagvolume
	:structsave(bufferptr,sftcmdoff)
	rts


// 0D - Pattern break
// ******************
// Tick 0
fx0patternbreak:	// Tick 0
	tax
	and #$0f
	sta temp
	txa
	lsr
	lsr
	lsr
	lsr
	tax
	lda mul10,x
	clc
	adc temp	
	sta curdivision
	ldy patternindex
	iny
	jsr setpattern
	lda #$01
	sta skipdivision
	rts	
	

// 0F - Change speed
// *****************
// Tick 0	
fx0changespeed:		// Tick 0
	bne !+
	rts
!:
	ldx #$01
	stx changetimer
	cmp #$21
	bcs !+	
	sta newspeed	
	rts
!:	
	sta bpm
	jsr calctimer
	sta timer
	stx timer+1	
	rts

	
// Extended "E" FXs
// ****************

// E1 - Fine slide up
// ******************
// Tick 0	
.label efx0fineslideup=fxnslideup


// E2 - Fine slide down
// ********************
// Tick 0	
.label efx0fineslidedown=fxnslidedown
	
	
// E3 - Set glissando mode
// ***********************
// Tick 0	
efxsetglissando:
	asl
	sta temp
	:structload(bufferptr,sftflgoff)
	and #$ff-$02
	ora temp
	:structsave(bufferptr,sftflgoff)
	rts
	
// E4 - Set vibrato waveform
// *************************
// Tick 0		
efx0setvibwave:		// Tick 0
	and #$07
	:structsave(bufferptr,sftvbwoff)
	rts

	
// E5 - Set fine tune
// ******************
// Tick 0			
efx0setfinetune:	// Tick 0
	pha
	:structload(bufferptr,sftsmpoff)	// Get sample number...
	tax
	pla
	sta sampleft-1,x					// ...for finetune		
	rts	
	
	
// E6 - Loop pattern
// *****************
// Tick 0				
efx0loop:			// Tick 0
	bne !+
	lda curdivision
	:structsave(bufferptr,sftlppoff)
	rts
!:	
	:structcmp(bufferptr,sftlpcoff)
	bne !+
	lda #$00
	:structsave(bufferptr,sftlpcoff)	
	rts
!:
	:structload(bufferptr,sftlpcoff)
	adc #$00
	:structsave(bufferptr,sftlpcoff)
	:structload(bufferptr,sftlppoff)
	sta curdivision	
	lda #$01
	sta skipdivision
	rts		


// E7 - Set tremolo waveform
// *************************
// Tick 0			
efxsettrmwave:		// Tick 0
	and #$07
	:structsave(bufferptr,sfttrwoff)
	rts


// E9 - Retrigger sample
// *********************
// Tick 0			
efx0noteretrigger:	// Tick 0
	:structsave(bufferptr,sfttksoff)
	:structload(bufferptr,sftcmdoff)
	:structsave(bufferptr,sftbkcoff)
	lda #$00
	:structsave(bufferptr,sfttkcoff)		
	jmp efxnnoteretrigger
	
efxnnoteretrigger:	// Tick 1
	:structload(bufferptr,sfttkcoff)
	:structcmp(bufferptr,sfttksoff)	
	bcs !+
	adc #$01
	:structsave(bufferptr,sfttkcoff)
	rts
!:	
	:structload(bufferptr,sftbkcoff)
	:structsave(bufferptr,sftcmdoff)
	lda #$00
	:structsave(bufferptr,sfttkcoff)
	rts

	
// EA - Fine slide volume up
// *************************
// Tick 0				
.label efx0volslideup=fxnvolumeslideexx	// Tick 0

	
// EB - Fine slide volume down
// ***************************
// Tick 0					
efx0volslidedown:	// Tick 0
	eor #$ff
	clc
	adc #$01
	jmp fxnvolumeslideexx

	
// EC - Cut note after x ticks
// ***************************
// Tick 0						
efx0cutnote:		// Tick 0
	:structsave(bufferptr,sfttksoff)
	lda #$00
	:structsave(bufferptr,sfttkcoff)
	jmp efxncutnote	

efxncutnote:		// Tick 1
	:structload(bufferptr,sfttkcoff)
	:structcmp(bufferptr,sfttksoff)
	bcs !+
	adc #$01
	:structsave(bufferptr,sfttkcoff)
	rts
!:
	bne !+	
	lda #$00
	:structload(bufferptr,sftvoloff)
	:structload(bufferptr,sftcmdoff)
	ora #flagvolume
	:structsave(bufferptr,sftcmdoff)
!:
	rts


// ED - Start note after x ticks
// *****************************
// Tick 0							
efx0startnote:		// Tick 0
	:structsave(bufferptr,sfttksoff)
	:structload(bufferptr,sftcmdoff)
	:structsave(bufferptr,sftbkcoff)
	lda #$00
	:structsave(bufferptr,sfttkcoff)	
	:structsave(bufferptr,sftcmdoff)
	jmp efxnstartnote	
	
efxnstartnote:		//Tick 1
	:structload(bufferptr,sfttkcoff)
	:structcmp(bufferptr,sfttksoff)
	bne !+
	tax
	:structload(bufferptr,sftbkcoff)
	:structsave(bufferptr,sftcmdoff)
	txa
!:
	clc
	adc #$01
	:structsave(bufferptr,sfttkcoff)
	rts
	

// EE - Delay pattern
// ******************
// Tick 0							
efx0delaypattern:	// Tick 0
	sta patdelay
	rts
	

// FX related functions
// ********************

// Slide to MAX
// ************
// >A,<X: MAX val
// 	   Y: Add val
slidetomax:
	sty temp			// Save value to be added
	sta temp2			// Save HI byte bound value
	stx temp3			// Save LO byte bound value
	sec
	txa									// Perform (Bound - CurrentValue)...	
	:structsbc(bufferptr,sftnrtoff)
	tax
	lda temp2	
	:structsbc(bufferptr,sftnrtoff+1)
	sta temp4
	bcc	stlimitreached					// If <0 limit reached
	txa									// Check if (Bound - CurrentValue)<AddValue
	cmp temp
	lda temp4
	sbc #$00
	bcc stlimitreached					// If so limit reached
	clc									// Ok, the addvalue can be added safely
	:structload(bufferptr,sftnrtoff)
	adc temp
	:structsave(bufferptr,sftnrtoff)
	tax
	:structload(bufferptr,sftnrtoff+1)
	adc #$00
	:structsave(bufferptr,sftnrtoff+1)
	tay
stsavevalue:
	:structload(bufferptr,sftcmdoff)
	ora #flagrate
	:structsave(bufferptr,sftcmdoff)
	rts		
stlimitreached:
	lda temp3
	:structsave(bufferptr,sftnrtoff)
	tax
	lda temp2
	:structsave(bufferptr,sftnrtoff+1)
	tay
	bcc stsavevalue
	
	
// Slide to MIN
// ************
// >A,<X: MIN val
// 	   Y: Sub val
slidetomin:
	sty temp			// Save value to be subtracted
	sta temp2			// Save HI byte bound value
	stx temp3			// Save LO byte bound value	
	sec									// Perform (CurrentValue - Bound)...	
	:structload(bufferptr,sftnrtoff)
	sbc temp3
	tax
	:structload(bufferptr,sftnrtoff+1)
	sbc temp2		
	sta temp4
	bcc	stlimitreached					// If <0 limit reached
	txa									// Check if (CurrentValue - Bound)<AddValue
	cmp temp
	lda temp4
	sbc #$00
	bcc stlimitreached					// If so limit reached										
	:structload(bufferptr,sftnrtoff)	// Ok, the subvalue can be subtracted safely
	sbc temp
	:structsave(bufferptr,sftnrtoff)
	tax
	:structload(bufferptr,sftnrtoff+1)
	sbc #$00
	:structsave(bufferptr,sftnrtoff+1)
	tay
	jmp stsavevalue
	

// Exchange glissando note with current
// ************************************	
xchgglissando:
/*
	:structload(bufferptr,sftnrtoff)
	tax
	:structload(bufferptr,sftglsoff)
	:structsave(bufferptr,sftnrtoff)
	txa
	:structsave(bufferptr,sftglsoff)
	:structload(bufferptr,sftnrtoff+1)
	tax
	:structload(bufferptr,sftglsoff+1)
	:structsave(bufferptr,sftnrtoff+1)
	txa
	:structsave(bufferptr,sftglsoff+1)
*/
	rts
	
	
// Tick FX jump table
// ******************
.align $80
.print "Tick FX jump table: $"+toHexString(*)
fxtablet0:	// First tick
.byte <fx0arpeggio,>fx0arpeggio				// 0 - OK - Arpeggio
.byte <fxnotimplemented,>fxnotimplemented	// 1 - OK
.byte <fxnotimplemented,>fxnotimplemented	// 2 - OK
.byte <fx0slidetonote,>fx0slidetonote		// 3 - OK - Slide to note
.byte <fx0vibrato,>fx0vibrato				// 4 - OK - Vibrato
.byte <fx0notvolslide,>fx0notvolslide		// 5 - OK - Slide to note + Volume slide
.byte <fx0volslidevib,>fx0volslidevib		// 6 - OK - Vibrato + Volume slide
.byte <fx0tremolo,>fx0tremolo				// 7 - OK - Tremolo
.byte <fx0setpanning,>fx0setpanning			// 8 - OK - Panning
.byte <fx0sampleoffset,>fx0sampleoffset		// 9 - OK - Sample offset
.byte <fx0volumeslide,>fx0volumeslide		// a - OK - Volume slide
.byte <fx0jumppattern,>fx0jumppattern		// b - OK - Jump pattern
.byte <fx0setvolume,>fx0setvolume			// c - OK - Set volume
.byte <fx0patternbreak,>fx0patternbreak		// d - OK - Pattern break
.byte <fxnotimplemented,>fxnotimplemented	// e - OK
.byte <fx0changespeed,>fx0changespeed		// f - OK - Set speed
// Extended (E) fx
.byte <fxnotimplemented,>fxnotimplemented	// 0 - Not supported (Hardware filtering)
.byte <efx0fineslideup,>efx0fineslideup		// 1 - OK - Fine portamento up
.byte <efx0fineslidedown,>efx0fineslidedown	// 2 - OK - Fine portamento down
.byte <efxsetglissando,>efxsetglissando		// 3 - OK - Set glissando mode
.byte <efx0setvibwave,>efx0setvibwave		// 4 - OK - Set vibrato waveform
.byte <efx0setfinetune,>efx0setfinetune		// 5 - OK - Set fine tune
.byte <efx0loop,>efx0loop					// 6 - OK - Loop pattern
.byte <efxsettrmwave,>efxsettrmwave			// 7 - OK - Set tremolo waveform
.byte <fxnotimplemented,>fxnotimplemented	// 8 - Not supported (there's 8xx)
.byte <efx0noteretrigger,>efx0noteretrigger	// 9 - OK - Note retrigger
.byte <efx0volslideup,>efx0volslideup		// a - OK - Fine volume slide up
.byte <efx0volslidedown,>efx0volslidedown	// b - OK - Fine volume slide down
.byte <efx0cutnote,>efx0cutnote				// c - OK - Cut note after n tick
.byte <efx0startnote,>efx0startnote			// d - Ok - Start note after n tick
.byte <efx0delaypattern,>efx0delaypattern	// e - OK - Delay pattern
.byte <fxnotimplemented,>fxnotimplemented	// f - Not supported (Invert sample)
fxtabletn:	// Ticks after first
.byte <fxnarpeggio,>fxnarpeggio				// 0 - OK - Arpeggio
.byte <fxnslideup,>fxnslideup				// 1 - OK - Portamento up
.byte <fxnslidedown,>fxnslidedown			// 2 - OK - Portamento down
.byte <fxnslidetonote,>fxnslidetonote		// 3 - OK - Slide to note
.byte <fxnvibrato,>fxnvibrato				// 4 - OK - Vibrato
.byte <fxnnotvolslide,>fxnnotvolslide		// 5 - OK - Slide to note + Volume slide
.byte <fxnvolslidevib,>fxnvolslidevib		// 6 - OK - Vibrato + Volume slide
.byte <fxntremolo,>fxntremolo				// 7 - OK - Tremolo
.byte <fxnotimplemented,>fxnotimplemented	// 8 - OK
.byte <fxnotimplemented,>fxnotimplemented	// 9 - OK
.byte <fxnvolumeslide,>fxnvolumeslide		// a - OK - Volume slide
.byte <fxnotimplemented,>fxnotimplemented	// b - OK
.byte <fxnotimplemented,>fxnotimplemented	// c - OK
.byte <fxnotimplemented,>fxnotimplemented	// d - OK
.byte <fxnotimplemented,>fxnotimplemented	// e - OK
.byte <fxnotimplemented,>fxnotimplemented	// f - OK
// Extended (E) fx
.byte <fxnotimplemented,>fxnotimplemented	// 0 - Not supported (Hardware filtering)
.byte <fxnotimplemented,>fxnotimplemented	// 1 - OK
.byte <fxnotimplemented,>fxnotimplemented	// 2 - OK
.byte <fxnotimplemented,>fxnotimplemented	// 3 - OK
.byte <fxnotimplemented,>fxnotimplemented	// 4 - OK
.byte <fxnotimplemented,>fxnotimplemented	// 5 - OK
.byte <fxnotimplemented,>fxnotimplemented	// 6 - OK
.byte <fxnotimplemented,>fxnotimplemented	// 7 - OK
.byte <fxnotimplemented,>fxnotimplemented	// 8 - Not supported (there's 8xx)
.byte <efxnnoteretrigger,>efxnnoteretrigger	// 9 - OK  - Note retrigger
.byte <fxnotimplemented,>fxnotimplemented	// a - OK
.byte <fxnotimplemented,>fxnotimplemented	// b - OK
.byte <efxncutnote,>efxncutnote				// c - OK  - Cut note after n tick
.byte <efxnstartnote,>efxnstartnote			// d - OK  - Start note after n tick
.byte <fxnotimplemented,>fxnotimplemented	// e - OK
.byte <fxnotimplemented,>fxnotimplemented	// f - Not supported (Invert sample)

mul10:
.fill 10,10*i

