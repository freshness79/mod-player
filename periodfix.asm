// Play timing constants

.const	c64palfreq		=	985248			// Pal frequency
.const	c64ntscfreq		=	1022727			// NTSC frequency

// Play timing computed constants

.var	timerdividend
.if (pal)
	.eval	timerdividend	=	2.5*c64palfreq		// C64 timer dividend
.if (!pal)
	.eval	timerdividend	=	2.5*c64ntscfreq	// C64 timer dividend

// DAC frequency constants

.const	amigapalfreq	=	7093789.2		// Amiga DAC frequency
//.const	amigapalfreq	=	7210000
.const	audiomodfreq	=	6250000			// Audio module DAC frequency


// DAC computed constants

.const	ftmultiplier	=	exp(log(2)/96)	// Fine tune freq multiplier
.const	periodratio		=	2*audiomodfreq/amigapalfreq			// Ratio between two frequency above (used to fix sample playback rate)
.const	fpperiodratio	=	floor([1<<fpprecbits]*periodratio)	// Fixed point version

	
// Get real period (amiga period)x(periodratio) in fixed point
// ***********************************************************	
// IN: <X,>Y => Period to transform
fixperiod:	
	stx ARG2
	sty ARG2+1
	lda #<fpperiodratio
	sta ARG1
	lda #>fpperiodratio
	sta ARG1+1
	sec				// Compute FT +1 - +7	
	jmp mult16x16	// Results in PRODUCT+2,+3
	
	
// Calculate timer value
// *********************
calctimer:	
	lda bpm		
	sta DIVISOR
	lda #0
	sta DIVISOR+1
	lda #[[timerdividend]&$ff]
	sta DIVIDEND
	lda #[[timerdividend>>8]&$ff]
	sta DIVIDEND+1
	lda #[[timerdividend>>16]&$ff]
	sta DIVIDEND+2	
	jsr divide24x16
	lda DIVIDEND
	ldx DIVIDEND+1	
	rts
		

// Get note from rate
// ******************
// >A,<X: rate
getnote:
	and #$03
	clc
	adc #>notelookup
	sta pointer+1
	stx pointer
	sty SPARE
	:structload(pointer,0)
	bpl !+
	lda #$00
!:
	ldy SPARE
	rts

	
// Get note from rate (Glissando)
// ******************************
// >A,<X: rate
getnotegls:
	and #$03
	clc
	adc #>notelookup
	sta pointer+1
	stx pointer
	sty SPARE
	:structload(pointer,0)	
	ldy SPARE
	rts
	
	
// Get rate from note
// ******************
// IN: X: note
//     A: finetune
// OUT: >A,<X
getrate:	
	ldy #$24
	dex
	jsr mul8x8
	clc
	lda PRODUCT+0
	adc #<finetunel
	sta pointer+2
	lda PRODUCT+1
	adc #>finetunel
	sta pointer+3
	clc
	lda PRODUCT+0
	adc #<finetuneh
	sta pointer
	lda PRODUCT+1
	adc #>finetuneh
	sta pointer+1
	txa	
	tay
	lda (pointer),y
	pha	
	lax (pointer+2),y		
	pla
	rts
			